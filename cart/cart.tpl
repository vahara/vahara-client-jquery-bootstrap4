<div class="vh-client-component-cart">

    <div class="modal vh-js-cart-modal vh-cart-modal">
        <div class="modal-content">
            <div class="vh-overlay vh-js-overlay">
                <img src="https://o2fdv.vahara.com/img/animated_spinner.gif">
            </div>
            <div class="modal-header">
                <p>Your Cart <span class="close vh-js-cart-modal-close vh-cart-modal-close">Close</span></p>
            </div>
            <div class="vh-modal-body-content vh-js-modal-body-content"></div>
            <div class="vh-all-total-box all-total-box">
                <div class="subtotal">Subtotal <span class="vh-js-cart-subtotal">$0</span></div>
                <div class="input-group vh-promo-box">
                    <input type="text" class="form-control vh-js-coupon-code" placeholder="Enter promo code">
                    <div class="input-group-append">
                        <button class="btn btn-primary vh-js-apply-remove-coupon" type="button">Apply</button>
                    </div>
                </div>
                <div class="discount-total">Discount <a href="javascript:void(0);" class="vh-js-remove-discount ml-2"> <i class="fa fa-remove"></i> Remove</a> <span class="vh-js-discount-total">$0</span></div>
                <div class="main-total">Total <span class="vh-js-cart-total">$0</span></div>
                <button class="btn btn-primary btn-block btn-lg vh-js-proceed-to-checkout" type="button" >Checkout</button>
            </div>
        </div>
    </div>


    <!-- TEMPLATES -->

    <!---------------------------
        Product (lineitem)
    -----------------------------
        Available variables
    -----------------------------
        id
        product_sku
        product_title
        product_description
        product_image
        product_price
        quantity
    -->
    <script class="vh-js-template-cart-lineitem" data-priority="100" type="text/x-handlebars-template">
        <div class="vh-js-order-product vh-order-product vh-js-order-product-{{id}} clearfix">
            <div class="close-box"><a class="vh-js-remove-cart-item" href="javascript:void(0);" data-id="{{id}}">X</a></div>
            <div class="img-box">
                <a href="{{{product_slug}}}"><img class="vh-product-image" src="{{{product_image}}}"></a>
            </div>
            <div class="about-product">
                <h6 class="vh-js-cart-product-title"><a href="{{{product_slug}}}">{{product_title}}</a></h6>
                <p class="vh-js-cart-product-description">{{product_cart_label}}</p>
                <div class="qty-box">
                    <a href="javascript:void(0);" class="vh-js-cart-decrement" data-id="{{id}}">-</a>
                    <input class="vh-js-cart-qty" type="text" placeholder="QTY {{quantity}}" disabled>
                    <a href="javascript:void(0);" class="vh-js-cart-increment" data-id="{{id}}">+</a>
                    <span class="vh-js-cart-product-amount">${{currency_with_comma (math product_price "*" quantity)}}</span>
                </div>
            </div>
        </div>
    </script>

</div>
