function VaharaCart($) {

    this.init = function() {
        console.log('VaharaCart Initialized!');

        var _templates = {lineItem: '.vh-js-template-cart-lineitem'};
        var _settings = [];
        var _addedProducts = [];
        var couponCode = null;

        /**
         * When show cart element is clicked
         *
         * Please note the universal selector
         */
        $(document).on('click', '.vh-js-show-cart', function (e) {
            e.preventDefault();
            $('.vh-js-cart-modal').modal({show: true, backdrop: 'static', keyboard: false});
        });

        this.showCart = function () {
          setTimeout(function () {
            $(".vh-js-cart-modal").modal({
              show: true,
              backdrop: "static",
              keyboard: false,
            });
          }, 10);
        };

        /**
         * Redirect the user to checkout page
         */
        $(document).on('click', '.vh-js-proceed-to-checkout', function () {
            return _goToCheckout();
        });

        $(document).on('click', '.vh-js-apply-remove-coupon', function (event) {
            event.preventDefault();
            var action = $(this).attr('data-action');
            return applyRemoveCoupon(action);
        });

        /**
         * Remove applied discount.
         */
        $(document).on('click', '.vh-js-remove-discount', function (event) {
            event.preventDefault();
            var action = 'remove';
            return applyRemoveCoupon(action);
        });

        /**
         * When modal close element is clicked
         */
        $(document).on('click', '.vh-js-cart-modal-close', function (e) {
            e.preventDefault();
            v.triggerEvent('vh-cart:close-after-update', _addedProducts);
            $('.vh-js-cart-modal').modal('hide');
        });

        $(document).on('click', '.vh-js-cart-decrement', function (e) {
            e.preventDefault();
            e.stopPropagation();

            var itemId = $(this).data('id');
            updateItemQuantity(itemId, 'D', e.currentTarget);
        });

        $(document).on('click', '.vh-js-cart-increment', function (e) {
            e.preventDefault();
            e.stopPropagation();

            var itemId = $(this).data('id');
            updateItemQuantity(itemId, 'I', e.currentTarget);
        });

        /**
         * When the product X (remove item) element is clicked
         */
        $(document).on('click', '.vh-js-remove-cart-item', function (e) {
            e.preventDefault();
            e.stopPropagation();

            var itemId = $(this).data('id');
            removeCartItem(itemId);
        });

        /**
         * When add to cart is pressed
         */
        $(document).on('click', '.vh-js-add-to-cart', function (e) {
            e.preventDefault();

            var el = $(e.currentTarget);
            var dataSet = el.data();
            dataSet.source = e.currentTarget;

            v.triggerEvent('vh-cart:request-add', dataSet);
        });

        /**
         * When add to cart plain link  is pressed
         */
        $(document).on('click', 'a', function (e) {
            var link = $(e.currentTarget).attr('href');
            var matches = link.match(/vh-add-(.*)/);
            if (matches) {
                e.preventDefault();

                if (isNaN(matches[1])) {

                    v.callApi('/cart/get-id-for-product-key', {
                        data: {product_key: matches[1]},
                        success: function (response) {
                            console.log(response);
                            if (response.success === true) {
                                var dataSet = {
                                    itemId: response.data.itemId,
                                    stock: '',
                                    source: ''
                                };
                                console.log('data:');
                                console.log(dataSet);
                                v.triggerEvent('vh-cart:request-add', dataSet);
                            } else {
                                console.log('Item not found!');
                            }
                        }
                    });

                } else {
                    var dataSet = {
                        itemId: matches[1],
                        stock: '',
                        source: ''
                    };
                }

                v.triggerEvent('vh-cart:request-add', dataSet);
            }
        });

        $(document).on('submit', '.vh-cart-form', function(e) {
            e.preventDefault();

            var itemIdEl = this.querySelector('.vh-cart-item-id');
            var itemQtyEl = this.querySelector('.vh-cart-item-qty');
            var itemNameEl = this.querySelector('.vh-cart-item-name');
            var itemPriceEl = this.querySelector('.vh-cart-item-price');
            var skipCartEl = this.querySelector('.vh-cart-skip-cart');
            var submitBtn = this.querySelector('[type="submit"]');

            if(itemIdEl !== null) {
                var itemId = parseInt(itemIdEl.value);
                var itemQty = itemQtyEl === null ? 1 : itemQtyEl.value;
                var itemName = itemNameEl === null ? null : itemNameEl.value;
                var itemPrice = itemPriceEl === null ? null : itemPriceEl.value;

                var dataSet = {
                    itemId: itemId,
                    quantity: itemQty,
                    name: itemName,
                    price: itemPrice,
                    source: submitBtn,
                    skipCart: skipCartEl !== null
                };

                v.triggerEvent('vh-cart:request-add', dataSet);

                return false;
            }

            return true;
        });

        /**
         * Handle the cart-empty event
         */
        v.onEvent('vh-cart:cart-empty', function () {
            console.log('vh-cart:cart-empty');
        });

        /**
         * Handle the add to cart event
         */
        v.onEvent('vh-cart:request-add', function (dataSet) {
            $.when(addOrUpdateCartItem(
                dataSet.itemId,
                dataSet.quantity,
                dataSet.stock,
                dataSet.name,
                dataSet.price,
                dataSet.source,
                dataSet.skipCart,
                dataSet.action,
                dataSet.note
        )).then(function (status) {
                console.log(status);
            });
        });

        /**
         * Handle the item-added event
         */
        v.onEvent('vh-cart:item-added', function (data) {
            addOrUpdateItem(data.item, data.source);

            if (data.action != 'D') {
                var contentData = {
                    value: data.item.product_price,
                    currency: 'USD',
                    quantity: 1,
                    price: data.item.product_price,
                    name: data.item.product_title + ' ' + data.item.product_cart_label,
                    item_id: data.item.id,
                    product_color: data.item.product_color,
                    product_size: data.item.product_size
                };
                v.triggerEvent('vh-cart:add-to-cart', {config: contentData});
            }
        });

        /**
         * Handle the item-updated event
         */
        v.onEvent('vh-cart:item-updated', function (data) {
            console.log('vh-cart:item-updated');

            addOrUpdateItem(data.item, data.source);
            if (data.action != 'D') {
                var contentData = {
                    value: data.item.product_price,
                    currency: 'USD',
                    quantity: 1,
                    price: data.item.product_price,
                    name: data.item.product_title + ' ' + data.item.product_cart_label,
                    item_id: data.item.id
                };
                v.triggerEvent('vh-cart:add-to-cart', {config: contentData});
            }

        });

        /**
         * Handle the cart closed event
         */
        v.onEvent('vh-cart:close-after-update', function (data) {
            console.log('vh-cart:close-after-update');
        });

        /**
         * Handle the item-removed event
         */
        v.onEvent('vh-cart:item-removed', function (data) {
            console.log('vh-cart:item-removed');

            removeItem(data.itemId);
        });

        /**
         * Handle the item-removed event
         */
        v.onEvent('vh-cart:items-removed', function (data) {
            console.log('vh-cart:items-removed');
            var items = data.items.id;
            for (var i in items) {
                removeItem(items[i]);
            }
            // removeItem(data.items);
        });

        /**
         * If we've received purchase limit message from the server
         */
        v.onEvent('vh-cart:item-purchase-limit-reached', function (data) {
            if(typeof window.vahara.Alert !== 'undefined') {
                window.vahara.Alert.alert('Error', 'You cannot add more of this item');
            } else {
                console.log('You cannot add more of this item');
            }

            if (data.source) {
                data.source.onClick = function() {return null;};
                $(data.source)
                    .addClass('disabled')
                    .attr('disabled', 'disabled')
                    .off('click');
            }
        });

        function applyRemoveCoupon(action) {

            //Apply coupon.
            if (action === 'add')  {
                var coupon = $(".vh-js-coupon-code").val();

                if (typeof coupon === "undefined" || coupon === '' || coupon === null) {
                    window.vahara.Alert.alert('Error', 'Promo code cannot empty. Please enter valid promo code');
                    return false;
                }

                v.callApi('/coupon/get-apply-coupon', {
                    data: {
                        visitor_id: v.getVisitorId(),
                        coupon: coupon
                    },
                    before: function () {
                        showLoading(true);
                    },
                    after: function () {
                        showLoading(false);
                    },
                    success: function (response) {
                        if (response.success === true) {
                            couponCode = coupon; //Assign the couponCode value.
                            //$(".vh-js-apply-remove-coupon").text('Remove').data('action', 'add'); // Change the button text
                            var products = response.data.items;
                            updateProductsDiscount(products);
                            applyDiscount(coupon);
                        } else {
                            window.vahara.Alert.alert('Error', response.message);
                            console.log(response.message);
                        }
                    }
                });
            } else if (action === 'remove') {
                //Remove coupon.
                v.callApi('/coupon/get-remove-coupon', {
                    data: {
                        visitor_id: v.getVisitorId()
                    },
                    before: function () {
                        showLoading(true);
                    },
                    after: function () {
                        showLoading(false);
                    },
                    success: function (response) {
                        console.log("Removing...");
                        console.log(response.data);
                        if (response.success === true) {
                            var products = response.data.items;
                            updateProductsDiscount(products);
                            applyDiscount(null);
                        } else {
                            window.vahara.Alert.alert('Error', response.message);
                            console.log(response.message);
                        }
                    }
                });
            }


        }

        function applyDiscount(coupon) {
            $(".vh-js-coupon-code").val(coupon);
            updateTotals();
        }

        function updateProductsDiscount(products) {
            for (var productIndex in products) {
                var product = products[productIndex];
                var addedProductIndex = _.findIndex(_addedProducts, function (o) {
                    return o.id.toString() === product.id.toString();
                });

                if (addedProductIndex !== -1) {
                    console.log("product");
                    console.log(product);
                    _addedProducts.splice(addedProductIndex, 1, product);
                }
            }
        }

        function _goToCheckout() {
            if (v.userId) {
                if (typeof _settings === 'object') {
                    showLoading(true);
                    setTimeout(function () {
                        if (_settings.store_checkout_page) {
                            window.location = '/' + _settings.store_checkout_page;
                        } else {
                            window.location = '/checkout';
                        }
                    }, 300);
                } else {
                    console.error('There was an error redirecting to checkout page.');
                }
            } else {
                /*var siteUrl = window.location.pathname;
                var pathParameters = siteUrl.split("/");
                var stylistName = pathParameters[1];
                var actionUrl = stylistName + '/checkout';
                v.jumpToAfterLogin = actionUrl;*/
                v.jumpToAfterLogin = '/checkout';
                window.vahara.triggerEvent('vh-login:show-guest-checkout-options');
                //window.vahara.triggerEvent('vh-login:show-login');
            }
        }

        /**
         * Update item quantity based on `+` and `-` on side cart
         *
         * @param itemId
         * @param action
         * @param sourceEl
         * @returns {boolean}
         */
        function updateItemQuantity(itemId, action, sourceEl) {
            if (typeof sourceEl === 'undefined') sourceEl = null;
            if (typeof action === 'undefined') action = 'I';
            if (typeof itemId === 'undefined' || itemId === null) return false;

            var product = _.find(_addedProducts, function (o) {
                return o.id.toString() === itemId.toString();
            });

            if (typeof product !== 'undefined') {
                var newQty = action === 'I' ? product.quantity + 1 : product.quantity - 1;
                if (newQty < 0) newQty = 0;

                // Just in case where we may have not changes the qty in any way
                if (product.quantity === newQty) return false;

                if (newQty > 0) {
                    addOrUpdateCartItem(itemId, newQty, null, null, null, sourceEl, false, action);
                } else {
                    removeCartItem(itemId);
                }
            }
        }

        /**
         * Add/Update an item on the side cart
         *
         * @param product
         * @param sourceEl
         */
        function addOrUpdateItem(product, sourceEl) {
            console.log(product);
            console.log(sourceEl);
            var addedProductIndex = _.findIndex(_addedProducts, function (o) {
                return o.id.toString() === product.id.toString();
            });

            if (addedProductIndex !== -1) {
                _addedProducts.splice(addedProductIndex, 1, product);
                updateProductDisplay(product, sourceEl);
            } else {
                _addedProducts.push(product);
                addProductDisplay(product, sourceEl);
            }
        }

        /**
         * Remove the item from side cart display
         *
         * @param itemId
         */
        function removeItem(itemId) {
            // Remove the item from _addedProducts
            _addedProducts = _.filter(_addedProducts, function (o) {
                return o.id.toString() !== itemId.toString();
            });

            if (_addedProducts.length <= 0) v.triggerEvent('vh-cart:cart-empty');

            $('.vh-client-component-cart .vh-js-modal-body-content .vh-js-order-product-' + itemId).remove();

            updateTotals();
        }

        /**
         * Fetch user cart items
         *
         * @param callback
         */
        function getCartItems(callback) {
            v.callApi('/cart/get-cart', {
                data: {visitor_id: v.getVisitorId()},
                before: function () {
                    showLoading(true);
                },
                after: function () {
                    showLoading(false);
                },
                success: function (response) {
                    if (response.success === true) {
                        _addedProducts = response.data.items;
                        couponCode = response.data.coupon;
                        if (typeof callback === 'function') callback(response.data.items);
                    } else {
                        console.log('No items added to cart!');
                    }
                }
            });
        }

        /**
         * Fetch user cart items
         *
         * @param callback
         */
        function getOutOfStockCartItems() {
            v.callApi('/cart/get-out-of-stock-cart-products', {
                data: {visitor_id: v.getVisitorId()},
                before: function () {
                    showLoading(true);
                },
                after: function () {
                    showLoading(false);
                },
                success: function (response) {
                    if (response.success === true) {
                        _removedProducts = response.data.items;

                        var html = '';
                        var items = [];
                        for (var i in _removedProducts) {
                            var count = parseInt(i) + 1;
                            items.push(_removedProducts[i].id);
                            html += '<b>' + _removedProducts[i].product_title + '</b><br>';
                        }

                        if (items.length > 1) {
                            html += " have been removed from your cart because they are out of stock";
                        } else {
                            html += " has been removed from your cart because it is out of stock";
                        }

                        if (items.length) {

                            if (html.length) {
                                window.vahara.Alert.alert('Warning', html);
                            }
                            removeCartItems(items);
                        }

                    } else {
                        console.log('No items removed from cart!');
                    }
                }
            });
        }

        /**
         * Adds/Updates an item
         *
         * @param _id
         * @param _qty
         * @param _stock
         * @param _name
         * @param _price
         * @param _sourceEl
         * @param _skipCart
         * @param _action - D for delete and replace quantity, I for increment quantity (I is default)
         * @param _note
         * @param callback
         * @returns {*|jQuery}
         */
        function addOrUpdateCartItem(_id, _qty, _stock, _name, _price, _sourceEl, _skipCart, _action, _note, callback) {
            var deferred = $.Deferred();

            if (typeof _stock === 'undefined' || _stock === null) _stock = 10;
            if (typeof _qty === 'undefined' || _qty === null) _qty = 1;
            if (typeof _skipCart === 'undefined' || _skipCart === null) _skipCart = false;
            if (typeof _action === 'undefined' || _action === null) _action = 'I';
            if (_qty < 0) _qty = 1;

            if (_id > 0 && _id !== 'undefined' && _id !== null) {
                var addedProduct = _.find(_addedProducts, function (o) {
                    return o.id.toString() === _id.toString();
                });

                var qty = _qty;
                if(_action !== 'D') {
                    qty = typeof addedProduct === 'undefined' ? _qty : addedProduct.quantity + 1;
                }

                var _customAttributes = {};
                if(typeof _name !== "undefined" && _name !== null && _name.length > 1) {
                    _customAttributes['name'] = _name;
                }
                if(typeof _price !== "undefined" && _price !== null && _price > -1) {
                    _customAttributes['price'] = _price;
                }
                if(typeof _note !== "undefined" && _note !== null && _note.length > 1) {
                    _customAttributes['note'] = _note;
                }

                v.callApi('/cart/add-to-cart', {
                    data: {
                        visitor_id: v.getVisitorId(),
                        item_id: _id,
                        quantity: qty,
                        custom_attributes: _customAttributes
                    },
                    before: function () {
                        showLoading(true);
                    },
                    after: function () {
                        showLoading(false);
                    },
                    success: function (response) {
                        if (response.success === true) {
                            if (response.message === 'added') {
                                couponCode = response.data.coupon;
                                applyDiscount(couponCode); // Set applied coupon and update total.

                                if(_skipCart) {
                                    return _goToCheckout();
                                }

                                // Try and find the index of the added element in _addedProducts
                                var localIndex = _.findIndex(_addedProducts, function (o) {
                                    return o.id.toString() === response.data.orderItem.item_id.toString();
                                });

                                // Make the _item definition standard
                                var _item = {
                                    id: response.data.orderItem.item_id,
                                    product_sku: response.data.orderItem.sku,
                                    product_title: response.data.orderItem.name,
                                    product_slug: response.data.orderItem.details.product_slug,
                                    product_cart_label: response.data.orderItem.details.product_cart_label,
                                    product_image: response.data.orderItem.image,
                                    product_price: response.data.orderItem.price,
                                    quantity: response.data.orderItem.quantity,
                                    discount: response.data.orderItem.discount,
                                    volume: response.data.orderItem.volume,
                                    custom_name_price: response.data.orderItem.details.custom_name_price ?
                                        response.data.orderItem.details.custom_name_price :
                                        false
                                };

                                // We've added/updated a product
                                var _fireEvent = 'vh-cart:item-added';

                                // If we do not have the item in _addedProducts,
                                // it means we have added the product for the first time
                                if (localIndex === -1) {
                                    // If we have found the item in _addedProducts,
                                    // it means we already have the product in cart
                                } else {
                                    _fireEvent = 'vh-cart:item-updated';
                                }

                                // Trigger the item-added or item-updated event
                                v.triggerEvent(_fireEvent, {
                                    item: _item,
                                    source: _sourceEl,
                                    action: _action,
                                    full_data: response.data
                                });

                                deferred.resolve('Item updated.');

                                if (callback) {
                                    callback(_item);
                                }
                            }
                        } else {

                            if(response.data.product_purchase_limit_reached) {
                                // Trigger the item-added or item-updated event
                                v.triggerEvent('vh-cart:item-purchase-limit-reached', {
                                    item: response.data.item_id,
                                    source: _sourceEl
                                });
                            }

                            deferred.reject('Could not add product!');
                        }
                    }
                });
            } else {
                deferred.reject('Invalid product.');
            }

            return deferred.promise();
        }

        /**
         * Remove a cart item
         *
         * @param itemId
         */
        function removeCartItem(itemId) {
            if (itemId > 0 && itemId !== 'undefined' && itemId !== null) {
                v.callApi('/cart/remove-from-cart', {
                    data: {
                        visitor_id: v.getVisitorId(),
                        item_id: itemId
                    },
                    before: function () {
                        showLoading(true);
                    },
                    after: function () {
                        showLoading(false);
                    },
                    success: function (response) {
                        if (response.success === true) {
                            // Trigger the item-removed event
                            v.triggerEvent('vh-cart:item-removed', {itemId: response.data.orderItem});
                        }
                    }
                });
            } else {
                console.error('Invalid product!');
            }
        }

        /**
         * Remove a out of stock cart item
         *
         * @param itemId
         */
        function removeCartItems(items) {
            v.callApi('/cart/remove-products-from-cart', {
                data: {
                    visitor_id: v.getVisitorId(),
                    items_id: items,
                },
                before: function () {
                    showLoading(true);
                },
                after: function () {
                    showLoading(false);
                },
                success: function (response) {
                    if (response.success === true) {
                        // Trigger the item-removed event
                        var removedItems = response.data.orderItems;
                        v.triggerEvent('vh-cart:items-removed', {items: removedItems});
                        if (_addedProducts.length <= 0) v.triggerEvent('vh-cart:cart-empty');
                    }
                }
            });
        }

        /**
         * Render the side cart display
         *
         * @param products
         */
        function renderCart(products) {
            var html = '';
            for (var i in products) {
                html += v.compileTemplate(_templates.lineItem, products[i]);
            }
            $('.vh-client-component-cart .vh-js-modal-body-content').html(html);
            updateTotals();
        }

        /**
         * Add product for the first time to the side cart display
         *
         * @param product
         * @param sourceEl
         */
        function addProductDisplay(product, sourceEl) {
            var html = v.compileTemplate(_templates.lineItem, product);
            $('.vh-client-component-cart .vh-js-modal-body-content').prepend(html);
            $('.vh-client-component-cart .vh-js-cart-modal').modal({show: true, backdrop: 'static', keyboard: false});

            updateTotals();
        }

        /**
         * Update product quantity on the side cart display
         *
         * @param product
         * @param sourceEl
         */
        function updateProductDisplay(product, sourceEl) {
            var updated = false;
            var html = v.compileTemplate(_templates.lineItem, product);
            var el = $('.vh-client-component-cart .vh-js-modal-body-content .vh-js-order-product-' + product.id);

            if (typeof sourceEl !== 'undefined' && sourceEl !== null) {
                var className = sourceEl.className;
                if (typeof className !== 'undefined' && className !== null && className.length > 0) {
                    if (className.indexOf('increment') !== -1 || className.indexOf('decrement') !== -1) {
                        updated = true;
                        el.after(html).remove();
                    }
                }
            }

            if (!updated) {
                el.remove();
                $('.vh-client-component-cart .vh-js-modal-body-content').prepend(html);
            }

            $('.vh-client-component-cart .vh-js-cart-modal').modal({show: true, backdrop: 'static', keyboard: false});

            updateTotals();
        }

        /**
         * Update cart totals
         */
        function updateTotals() {
            var total = 0;
            var totalItems = 0;
            var totalDiscount = 0;

            if (_addedProducts.length > 0) {
                for (var i in _addedProducts) {
                    total += _addedProducts[i].product_price * _addedProducts[i].quantity;
                    totalItems += _addedProducts[i].quantity;
                    totalDiscount = _addedProducts[i].discount * _addedProducts[i].quantity;
                }
                var carticon = $('.vh-js-show-cart');
                carticon.css('display', 'inline-block');
            } else {
                var carticon = $('.vh-js-show-cart');
                carticon.css('display', 'none');
            }

            $('.vh-js-cart-qty').text(totalItems);
            $('.vh-js-cart-subtotal').html('$' + v.currencyWithComma(total.toFixed(2)));

            //Discount row if discount amount is greater than 0 else hide it.
            if (totalDiscount > 0) {
                $(".discount-total").show(); // Show 'Discount' row.
                $(".vh-promo-box").hide(); // Hide coupon text field.
                $(".discount-total").find('.vh-js-discount-total').text('$'+v.currencyWithComma(totalDiscount.toFixed(2)));
            } else {
                $(".discount-total").hide(); // Hide 'Discount' row.
                $(".vh-promo-box").show(); // Show promo box to enter coupon again.
            }


            if (total && totalDiscount) {
                total = total - totalDiscount;
            }
            $('.vh-js-cart-total').html('$' + v.currencyWithComma(total.toFixed(2)));

            //If there is coupon applied then add some attributes to 'Apply' coupon button.
            var coupon = $(".vh-js-coupon-code").val();
            if (coupon && totalDiscount) {
                $(".vh-js-apply-remove-coupon").text('Remove').attr('data-action','remove'); // Change the button text 'Remove'
            } else {
                $(".vh-js-apply-remove-coupon").text('Apply').attr('data-action','add'); // Change the button text 'Apply'
            }

            v.triggerEvent('vh-cart:totals-updated', {
                qty: totalItems,
                subtotal:total.toFixed(2),
                total:total.toFixed(2)
            });
        }

        /**
         * Show/Hide loading overlay
         *
         * @param show
         */
        function showLoading(show) {
            if (typeof show === 'undefined') show = true;
            if (show) {
                $('.vh-client-component-cart').addClass('loading');
            } else {
                setTimeout(function () {
                    $('.vh-client-component-cart').removeClass('loading')
                }, 300);
            }
        }

        // Get cart items when loaded
        getCartItems(function (products) {
            getOutOfStockCartItems();
            _settings = v.settings;
            renderCart(products);
        });

        this.renderCart = function(callback) {
            getCartItems(function (products) {
                getOutOfStockCartItems();
                _settings = v.settings;
                renderCart(products);

                if (callback) {
                    callback();
                }
            });
        }

        // Add an item to cart or update quantity then run callback if needed
        this.simpleItemUpdate = function (itemId, quantity, callback) {
            v.callApi("/cart/add-to-cart", {
                data: {
                    visitor_id: v.getVisitorId(),
                    item_id: itemId,
                    quantity: quantity,
                },
                success: function (response) {
                    window.vaharaCart.renderCart();
                    $(".vh-js-cart-qty").html(response.data.totalInCart);
                    if (callback) {
                        callback(response);
                    }
                },
            });
        }

        // Add multiple items or update quantities then run callback if needed
        // items should be an array of id:quantity, like: {2837: 5, 398348: 6}
        this.simpleMultipleUpdate = function (items, callback) {
            v.callApi("/cart/add-multiple-to-cart", {
                data: {
                    visitor_id: v.getVisitorId(),
                    items: JSON.stringify(items)
                },
                success: function (response) {
                    window.vaharaCart.renderCart();
                    $(".vh-js-cart-qty").html(response.data.totalInCart);
                    if (callback) {
                        callback(response);
                    }
                },
            });
        }

        // Get cart items when loaded
        this.renderCart();
    };
}

function initVaharaCart() {
    $$ = window.jQuery;

    if (!window.vahara.clientJqueryBootstrap4Path) {
        var scriptParts = window.vhCartCurrentScript.split('/');
        window.vahara.clientJqueryBootstrap4Path = scriptParts.slice(0, scriptParts.length - 2).join('/');
    }

    var root = $$('.vh-element-cart');
    var path = window.vahara.clientJqueryBootstrap4Path;

    if (window.vaharaLibsResourcePath) {
        path = window.vaharaLibsResourcePath;
    }

    if (window.vaharaCartResourcePath) {
        path = window.vaharaCartResourcePath;
    } else {
        path += '/cart';
    }
    var template = path + '/cart.tpl';
    if (window.vaharaCartHtmlLocation) {
        template = window.vaharaCartHtmlLocation;
    }
    var css = path + '/cart.css';

    // If there is no root for the cart then add one
    if(!root.length > 0) {
        $('body').append('<div class="vh-element vh-element-cart"></div>');
        root = $('.vh-element-cart');
    }

    if(root.length > 0) {
        if (!window.vaharaLibsNoAutoLoadCss && !window.vaharaCartLibsNoAutoLoadCss) {
            $('head').append('<link id="client_cart_css" rel="stylesheet" href="' + css + '" type="text/css">');
        }
        if (!window.vaharaLibsNoAutoRun && !window.vaharaCartNoAutoRun) {
            if (!window.vaharaLibsNoAutoLoadHtml && !window.vaharaCartLibsNoAutoLoadHtml) {
                $.get(template, function (data) {
                    root.empty().append(data);

                    window.vaharaCart = new VaharaCart($);
                    window.vaharaCart.init();
                });
            } else {
                window.vaharaCart = new VaharaCart($);
                window.vaharaCart.init();
            }
        }
    }

    window.removeEventListener('load', initVaharaCart, false);
}

window.vaharaClientJqueryBootstrap4CartPath = window.vahara.getCurrentScriptPath(-2);
window.vhCartCurrentScript = document.currentScript.src;
window.addEventListener('load', initVaharaCart, false);
