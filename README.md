# vahara-client-jquery-boostrap4

These is the official client library for Vahara for sites using jQuery and Bootstrap 4. These are made available under the CC0 license: https://tldrlegal.com/license/creative-commons-cc0-1.0-universal

To load these libraries, be sure to first include your vahara start script, then require the javascript for these. Required CSS and HTML templates will be loaded automatically.

If you would like to replace the CSS or HTML with your own, or if you would like to extend the javascript and run it via your own libraries, the following configuration parameters can be set before the tags that load the javascript for these libraries:

```
window.vaharaLibsNoAutoLoadCss = 1; // don't load the CSS for any Vahara library
window.vaharaLibsNoAutoLoadHtml = 1; // don't load the HTML for any Vahara library
window.vaharaLibsNoAutoRun = 1; // don't init any Vahara library automatically
window.vaharaLibsResourcePath = ''; // define a path where the .tpl and .css files should load from (for example, if you set it to '/custom' then the auth css would load from '/custom/auth/auth.css' and the cart html from '/custom/cart/cart.tpl'

// These can be specified for any library-- Auth, Cart, Checkout, etc:

window.vaharaAuthNoAutoRun = 1; // don't automatically run the auth library -- (new VaharaClientJqueryBootstrapAuth($$)).init(); will need to be run elsewhere to kick off auth functionality
window.vaharaAuthNoAutoLoadCss = 1; // don't load the CSS for the "auth" component (replace "Auth" with any other component you don't want to load the CSS for)
window.vaharaAuthNoAutoLoadHtml = 1; // don't load the HTML for the "auth" component (replace "Auth" with any other component you don't want to load the HTML for)
window.vaharaAuthResourcePath = ''; // specify a different path to load the auth css and html from, like '/custom/auth'
window.vaharaAuthResourcePath = ''; // define a path where the .tpl and .css files should load from (for example, if you set it to '/custom' then the auth css would load from '/custom/auth/auth.css' and the cart html from '/custom/cart/cart.tpl'
```

