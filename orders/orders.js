function VaharaOrders($) {
    this.init = function () {
        console.log('VaharaOrders Initialized!');

        let _templates = {
            orderDetails: '.vh-js-partial-order-details',
            ordersTableCommon: '.vh-js-partial-orders-table-common'
        };
        let fetchNewOrdersThresholdSeconds = 10;
        let poLastFetched = null;
        let coLastFetched = null;

        // Fetch user orders data
        function fetchUserOrders(reFetch) {
            if (typeof reFetch === 'undefined') reFetch = false;
            if (!reFetch) {
                reFetch = poLastFetched === null ? true : moment().diff(poLastFetched, 'seconds') > fetchNewOrdersThresholdSeconds;
            }
            if (reFetch === true) {
                v.callApi('/orders/list-user-orders', {
                    success: function (response) {
                        if (response.success) {
                            poLastFetched = new moment();
                            if (response.data.orders.length > 0) {
                                listOrders('.vh-client-component-orders .tab-pane.vh-js-po', response.data);
                            } else {
                                console.error('error', 'No user orders yet.');
                            }
                        } else {
                            console.error('error', 'There was an error retrieving order list. Please try later!');
                        }
                    }
                });
            }
        }

        function fetchSellerOrders(reFetch) {
            if (typeof reFetch === 'undefined') reFetch = false;
            if (!reFetch) {
                reFetch = coLastFetched === null ? true : moment().diff(coLastFetched, 'seconds') > fetchNewOrdersThresholdSeconds;
            }
            if (reFetch === true) {
                v.callApi('/orders/list-seller-orders', {
                    success: function (response) {
                        if (response.success) {
                            coLastFetched = new moment();
                            if (response.data.orders.length > 0) {
                                listOrders('.vh-client-component-orders .tab-pane.vh-js-co', response.data);
                            } else {
                                console.error('error', 'No seller orders yet.');
                            }
                        } else {
                            console.error('error', 'There was an error retrieving order list. Please try later!');
                        }
                    }
                });
            }
        }

        function listOrders(targetContainer, data) {
            if (typeof targetContainer === 'undefined') throw Error('No container specified.');
            if (typeof data === 'undefined') throw Error('No data specified.');

            let html = v.compileTemplate(_templates.ordersTableCommon, data);

            $(targetContainer).html(html);
        }

        function fetchOrderDetails(orderId) {
            // Fetch user order details
            if (typeof orderId === 'undefined' || orderId <= 0) {
                console.error('error', 'Invalid order id.');
                return false;
            }

            v.callApi('/orders/details', {
                data: {order_id: orderId},
                success: function (response) {
                    if (response.success === true) {
                        $('.portlet.vh-order-list').hide();
                        let html = v.compileTemplate('.vh-js-partial-order-details', response.data.order);
                        $('.portlet.vh-order-details').html(html).show();
                    } else {
                        window.location.hash = '#!list';
                        window.vahara.Alert.alert('Error', response.message);
                        console.error('error', 'There was an error retrieving order details. Please try later!');
                    }
                }
            });
        }

        $(document).on('click', '.vh-client-component-orders .vh-js-tab-link', function (e) {
            if ($(this).data('type') === 'PO') {
                fetchUserOrders();
            } else if ($(this).data('type') === 'CO') {
                fetchSellerOrders();
            }
        });

        $(window).on('hashchange', function () {
            let hash = window.location.hash.replace(/^#!/, '');
            hash = hash.split('=', 2);
            switch (hash[0]) {
                case 'list':
                    $('.portlet.vh-order-details').hide();
                    $('.portlet.vh-order-list').show();
                    break;

                case 'view':
                    let orderId = parseInt(hash[1]);
                    fetchOrderDetails(orderId);
                    break;
            }
        }).trigger('hashchange');

        fetchUserOrders(true);
    };
}

function initVaharaOrders() {
    $$ = window.jQuery;

    if (!window.vahara.orderJqueryBootstrap4Path) {
        var scriptParts = window.vhOrdersCurrentScript.split('/');
        window.vahara.clientJqueryBootstrap4Path = scriptParts.slice(0, scriptParts.length - 2).join('/');
    }

    var root = $$('.vh-element-orders');
    var path = window.vahara.clientJqueryBootstrap4Path;

    if (window.vaharaLibsResourcePath) {
        path = window.vaharaLibsResourcePath;
    }

    if (window.vaharaCheckoutResourcePath) {
        path = window.vaharaCheckoutResourcePath;
    } else {
        path += '/orders';
    }
    var template = path + '/orders.tpl';
    var css = path + '/orders.css';

    if (root.length > 0) {
        if (!window.vaharaLibsNoAutoLoadCss && !window.vaharaCheckoutLibsNoAutoLoadCss) {
            $$('head').append('<link id="client_orders_css" rel="stylesheet" href="' + css + '" type="text/css">');
        }

        if (!window.vaharaLibsNoAutoRun && !window.vaharaCheckoutNoAutoRun) {
            if (!window.vaharaLibsNoAutoLoadHtml && !window.vaharaCheckoutLibsNoAutoLoadHtml) {
                $$.get(template, function (data) {
                    root.empty().append(data);

                    (new VaharaOrders($$)).init();
                });
            } else {
                (new VaharaOrders($$)).init();
            }
        }
    }

    window.removeEventListener('load', initVaharaOrders, false);
}

window.vhOrdersCurrentScript = document.currentScript.src;

window.addEventListener('load', initVaharaOrders, false);
