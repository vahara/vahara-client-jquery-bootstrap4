<div class="vh-client-component-orders">

    <!-- Order List -->
    <div class="portlet vh-order-list">
        <div class="head mb-4">
            <div class="title">
                <ul class="nav nav-pills">
                    <li class="nav-item">
                        <a class="vh-js-tab-link nav-link active"
                                data-type="PO"
                                data-toggle="pill"
                                href=".vh-js-po"
                                role="tab"
                                aria-controls="personal_orders"
                                aria-selected="true">
                            MY PERSONAL ORDERS
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="vh-js-tab-link nav-link"
                                data-type="CO"
                                data-toggle="pill"
                                href=".vh-js-co"
                                role="tab"
                                aria-controls="customer_orders"
                                aria-selected="false">
                            MY CUSTOMER ORDERS
                        </a>
                    </li>
                </ul>
            </div>
            <div class="tools"></div>
        </div>
        <div class="body">
            <div class="tab-content" id="pills-tabContent">
                <div class="vh-js-po tab-pane fade show active" role="tabpanel" aria-labelledby="personal_orders-tab"></div>
                <div class="vh-js-co tab-pane fade" role="tabpanel" aria-labelledby="customer_orders-tab"></div>
            </div>
        </div>
    </div>
    <!-- /Order List -->

    <!-- Order View Wrapper -->
    <div class="portlet vh-order-details"></div>
    <!-- /Order View Wrapper -->


    <!------------------------------------
        Orders Table
    --------------------------------------
        Available variables
    --------------------------------------
        {
            column_headers: [{}],
            orders: [{
                id,
                order_date,
                order_id,
                buyer_name,
                buyer_email,
                total,
                order_status
            }]
        }
    -->
    <script class="vh-js-partial-orders-table-common" data-priority="100" type="text/x-handlebars-template">
        <table class="table table-bordered table-striped">
            <thead>
                <tr class="vh-table-headers">
                    {{#each column_headers}}
                        <th>{{this}}</th>
                    {{/each}}
                </tr>
            </thead>
            <tbody class="vh-table-rows vh-js-table-rows">
                {{#each orders}}
                    <tr id='row-{{id}}'>
                        <td>{{order_date}}</td>
                        <td>{{order_id}}</td>
                        <td>{{buyer_name}}</td>
                        <td>{{total}}</td>
                        <td>{{order_status}}</td>
                        <td><a href="#!view={{id}}" class="vh-js-vieworder">see details</a></td>
                    </tr>
                {{/each}}
            </tbody>
        </table>

    </script>

    <!------------------------------------
        Order Details
    --------------------------------------
        Available variables
    --------------------------------------
        {
            'id'
            'order_id'
            'user_id'
            'subtotal'
            'tax'
            'discount'
            'total'
            'buyer_name'
            'buyer_email'
            'order_status'
            'order_date'
            'details'
            'line_items' => [{
                'id'
                'sku'
                'name'
                'cart_label'
                'image'
                'price'
                'total_price'
                'quantity'
                'discount'
                'volume'
            }, ]
            'shipping' => [
                'rate'
                'method_code'
                'method_label'
                'address_id'
                'first_name'
                'last_name'
                'address_1'
                'address_2'
                'city'
                'state'
                'zip'
                'country'
                'phone'
            ],
            'billing' => [
                'address_id'
                'first_name'
                'last_name'
                'address_1'
                'address_2'
                'city'
                'state'
                'zip'
                'country'
                'phone'
            ],
            'payment' => [
                'method_code'
                'method_label'
                'transaction_id'
            ]
        }
    -->
    <script class="vh-js-partial-order-details" data-priority="100" type="text/x-handlebars-template">
        <div class="body">
            <a class="vh-js-orders_link text-secondary" href="#!list">⟵ All Orders</a>
            <div class="row mt-5">
                <div class="col-md-2">Order No:</div>
                <div class="col-md-10 font-weight-bold">{{order_id}}</div>
                <div class="col-md-2">Date:</div>
                <div class="col-md-10 font-weight-bold">{{order_date}}</div>
                <div class="col-md-2">Status:</div>
                <div class="col-md-10 font-weight-bold">{{order_status}}</div>
            </div>
            <div class="row mt-4">
                {{#if should_ship}}
                <div class="col-md-4 col-sm-12 mb-5">
                    <h5>Shipping Address</h5>
                    <div class="vh-address-block">
                        {{#with shipping}}
                            <div class="name mt-3">{{first_name}} {{last_name}}</div>
                            <div class="address">
                                <span class="line">{{address_1}}</span>
                                {{#if address_2}}
                                    <span class="line">{{address_2}}</span>
                                {{/if}}
                                <span class="line">{{city}}, {{state}}</span>
                                <span class="line">{{zip}}</span>
                            </div>
                            {{#if phone}}
                                <div class="phone">{{phone}}</div>
                            {{/if}}
                        {{/with}}
                    </div>
                </div>
                {{/if}}
                <div class="col-md-4 col-sm-12 mb-5">
                    <h5>Billing Address</h5>
                    <div class="vh-address-block">
                        {{#with billing}}
                            <div class="name mt-3">{{first_name}} {{last_name}}</div>
                            <div class="address">
                                <span class="line">{{address_1}}</span>
                                {{#if address_2}}
                                    <span class="line">{{address_2}}</span>
                                {{/if}}
                                <span class="line">{{city}}, {{state}}</span>
                                <span class="line">{{zip}}</span>
                            </div>
                            {{#if phone}}
                                <div class="phone">{{phone}}</div>
                            {{/if}}
                        {{/with}}
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 mb-5">
                    <h5>Payment Details</h5>
                    <div class="type mt-3">Method: {{payment.method_label}}</div>
                    <div class="card-no">Transaction Ref# {{payment.transaction_id}}</div>
                </div>
                {{#if should_ship}}
                <div class="col-md-4 col-sm-12 mb-5">
                    <h5>Shipping Mode</h5>
                    <div class="type mt-3">{{shipping.method_label}}</div>
                    <div class="card-no">
                        <a class="vh-js-shipping_track_link text-secondary font-italic" href="javascript:void(0);"><u>Track this shipment</u></a>
                    </div>
                </div>
                {{/if}}
            </div>

            <div class="row">
                <div class="table-responsive w-100">
                    <table class="table table-bordered order-table">
                        <thead>
                        <tr>
                            <th>Item SKU(s)</th>
                            <th>Name</th>
                            <th>Quantity</th>
                            <th>Unit Price</th>
                            <th>Total Price</th>
                        </tr>
                        </thead>
                        <tbody class="vh-js-items_list">
                            {{#each line_items}}
                                <tr>
                                    <td>{{sku}}</td>
                                    <td>
                                        <img src="{{{image}}}" class="rounded float-left" style="width:100px">
                                        <span class="vh-order-product-title ml-4">{{name}}</span>
                                    </td>
                                    <td>
                                        <span class="vh-order-product-quantity">{{quantity}}</span>
                                    </td>
                                    <td class="vh-currency">
                                        <span class="vh-order-product-price">${{currency price}}</span><br>
                                    </td>
                                    <td class="vh-currency">
                                        <span class="vh-order-product-unittotal">${{currency total_price}}</span>
                                    </td>
                                </tr>
                            {{/each}}
                            <tr>
                                <td class="vh-currency" colspan="4">Subtotal</td>
                                <td class="vh-currency">${{currency subtotal}}</td>
                            </tr>
                            {{#gtZero discount}}
                                <tr class="total-saving">
                                    <td class="vh-currency" colspan="4">Saving</td>
                                    <td class="vh-currency">${{currency discount}}</td>
                                </tr>
                            {{/gtZero}}
                            {{#gtZero tax}}
                                <tr>
                                    <td class="vh-currency" colspan="4">Taxes</td>
                                    <td class="vh-currency">${{currency tax}}</td>
                                </tr>
                            {{/gtZero}}
                            {{#if should_ship}}
                                <tr>
                                    <td class="vh-currency" colspan="4">Shipping</td>
                                    <td class="vh-currency">${{currency shipping.rate}}</td>
                                </tr>
                            {{/if}}
                            <tr class="total-amount">
                                <td class="vh-currency" colspan="4">Total</td>
                                <td class="vh-currency">${{currency total}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row mt-5">
                <div class="col-md-12">
                    <a class="btn btn-outline-secondary pull-right mr-2" href="#!list">⟵ All Orders</a>
                </div>
            </div>
        </div>
    </script>

</div>
