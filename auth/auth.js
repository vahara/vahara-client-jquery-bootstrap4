function VaharaAuth($) {

    /** EXPOSED CLASS **/
    this.init = function () {
        console.log('VaharaAuth Initialized!');
        $('.vh-js-overlay').hide();

        var uiItems = {
            root: '.vh-client-component-auth',
            dsFields: '.vh-js-template-ds-fields',
            loginBtn: '.vh-js-login-action',
            logoutBtn: '.vh-js-logout-action',
            loginBtnAlwaysVisible: '.vh-js-login-action-always-visible',
            logoutBtnAlwaysVisible: '.vh-js-logout-action-always-visible',
            loginLogoutBtn: '.vh-js-login-logout-action',
            parentModal: '.vh-js-authreg-modal',
            loginModal: '.vh-js-login-box',
            registerModal: '.vh-js-page-signup-box',
            restePasswordModal: '.vh-js-reset-password-box',
            updatePasswordModal: '.vh-js-update-password-box',
            accessCodeVerificationModal: '.vh-js-code-verification-box',
            successMsgContainer: '.vh-js-success',
            errorMsgContainer: '.vh-js-errors',
            dsSponsorField: '.vh-js-template-ds-sponsor-field',
            dsPhoneField: '.vh-js-template-ds-phone-field'
        };

        var fvInstances = {
            login: null,
            register: null,
            resetPassword: null,
            updatePassword: null,
            accessCodeVerification: null
        };

        var _settings = null;
        var _msgTimeout = null;
        var _updatePasswordToken = null;

        /* START PRIVATE FUNCTIONS & VARIABLES */
        var _isLoggedIn = false;
        var _initialized = false;
        var _firstLoad = true;

        var verifyOtp = 0;

        var _showMessage = function (type, message) {
            if (typeof type === 'undefined') type = 'success';
            clearTimeout(_msgTimeout);
            $(uiItems.errorMsgContainer, uiItems.successMsgContainer).hide();
            var msgBox = type === 'error' ? uiItems.errorMsgContainer : uiItems.successMsgContainer;
            $(msgBox).text(message).slideDown();
            _msgTimeout = setTimeout(function () {
                $(msgBox).slideUp();
            }, 5000);
        };

        var _doLoginAction = function (data, redirectTo) {
            var user = data.user;
            v.userId = user.id;
            v.user = data.user;
            v.dataToken = data.token;

            if (typeof redirectTo !== 'undefined' && user.guest_account) {
                v.guestUser = true;
                v.userId = null;
                v.user = null;
                v.dataToken = null;
                window.location = redirectTo;

                return false;
            }

            if (window.vaharaAuthTriggerLocalLogin) {
                // Call the local site to have it verify that the user
                // is logged in, and at what level.
                v.jQuery.ajax('/login_callback', {
                    data: {token: data.token},
                    success: function (finalData) {
                        v.triggerEvent('vh-login:login-callback-complete');
                        v.triggerEvent('vh-login:status-updated');
                        v.triggerEvent('vh-login:logged-in');
                    },
                    error: function () {
                        v.triggerEvent('vh-login:login-callback-complete');
                        v.triggerEvent('vh-login:status-updated');
                    }
                });
            } else {
                v.triggerEvent('vh-login:login-callback-complete');
                v.triggerEvent('vh-login:status-updated');
                v.triggerEvent('vh-login:logged-in');
            }

            if (v.jumpToAfterLogin) {
                var actionUrlToJump = v.jumpToAfterLogin;
                v.jumpToAfterLogin = null;

                // Give the browser a little time to notify the local site of the login before redirecting
                setTimeout(function () {
                    window.location = actionUrlToJump;
                }, 1000);
            }
        };

        var _showModal = function (vhModalClass) {
            _setRegStatus();
            var parentModal = $(uiItems.parentModal);
            if (!parentModal.is(':visible')) parentModal.modal({show: true, backdrop: 'static', keyboard: false});
            var modal = $(vhModalClass);
            if (modal.length) {
                $(uiItems.errorMsgContainer, uiItems.successMsgContainer).hide();
                $('.vh-js-page-modal-box').hide();

                _resetForms();
                modal.show();

                if (vhModalClass === uiItems.updatePasswordModal) {
                    fvInstances.updatePassword = _initFormValidation('.vh-js-update-password-form', {
                        'vh-js-up-password': {
                            validators: {
                                notEmpty: {message: 'Password is required'},
                                stringLength: {
                                    min: 6,
                                    max: 16,
                                    message: 'Password must be more than 6 and less than 16 characters long'
                                },
                            }
                        },
                        'vh-js-up-password-confirmation': {
                            validators: {
                                notEmpty: {message: 'Password confirmation is required'},
                                identical: {
                                    compare: function () {
                                        return $('.vh-js-update-password-form').find('[name="vh-js-up-password"]').val()
                                    },
                                    message: 'The password and its confirmation should be the same'
                                }
                            }
                        }
                    }, function () {
                        if (_updatePasswordToken == null) {
                            _showMessage('error', 'Invalid operation attempted!');
                            return false;
                        }

                        var form = $('.vh-js-update-password-form');
                        v.callApi('/auth/update-password', {
                            data: {
                                token: _updatePasswordToken,
                                password: form.find('.vh-js-up-password').val(),
                                confirm_password: form.find('.vh-js-up-password-confirmation').val(),
                            },
                            success: function (response) {
                                if (response.success) {
                                    _showMessage('success', response.message);
                                    form.hide('show', function () {
                                        $(this).remove();
                                    });
                                    setTimeout(function () {
                                        $(uiItems.parentModal).modal('hide');
                                    }, 4000);
                                } else {
                                    _showMessage('error', response.message);
                                }
                            }
                        });
                    });
                }

                if (vhModalClass === uiItems.accessCodeVerificationModal) {
                    fvInstances.accessCodeVerification = _initFormValidation('.vh-js-code-verification-form', {
                        'vh-js-code': {
                            validators: {
                                notEmpty: {message: 'Access code is required'}
                            }
                        }
                    }, function () {
                        var form = $('.vh-js-code-verification-form');
                        var responseData = window.userResponse;
                        var userId = responseData.user.id;
                        v.callApi('/auth/validate-access-code', {
                            data: {
                                code: form.find('.vh-js-code').val(),
                                userId: userId
                            },
                            success: function (response) {
                                if (response.success) {
                                    _showMessage('success', 'Access code verified');
                                    setVaharaBrowserValidatedCookie('vahara_browser_validated',userId,'/');
                                    setTimeout(function () {
                                        _doLoginAction(response.data);
                                    },1000);
                                } else {
                                    _showMessage('error', response.message);
                                }
                            }
                        });
                    });
                }

                if (vhModalClass === uiItems.restePasswordModal) {
                    fvInstances.resetPassword = _initFormValidation('.vh-js-reset-password-form', {
                        'vh-js-reset-email': {
                            validators: {
                                emailAddress: {message: 'Valid E-Mail address is required'},
                                notEmpty: {message: 'E-Mail is required'}
                            }
                        }
                    }, function () {
                        var form = $('.vh-js-reset-password-form');
                        v.callApi('/auth/send-reset-password', {
                            data: {
                                email: form.find('.vh-js-reset-email').val()
                            },
                            success: function (response) {
                                if (response.success) {
                                    _showMessage('success', response.message);
                                } else {
                                    _showMessage('error', response.message);
                                }
                            }
                        });
                    });
                }

                if (vhModalClass === uiItems.loginModal) {
                    var authKeyValidators = {
                        notEmpty: {message: 'E-Mail is required'}
                    };

                    authKeyValidators.emailAddress = {message: 'Valid E-Mail address is required'};

                    fvInstances.login = _initFormValidation('.vh-js-login-form', {
                        'vh-js-username': {
                            validators: authKeyValidators
                        },
                        'vh-js-password': {
                            validators: {
                                notEmpty: {message: 'Password is required'}
                            }
                        }
                    }, function () {
                        var browser_token = doesVaharaBrowserValidatedCookieExist('two_fa_token');
                        var form = $('.vh-js-login-form');

                        v.callApi('/auth/login', {
                            data: {
                                email: form.find('.vh-js-username').val(),
                                password: form.find('.vh-js-password').val(),
                                two_fa_token: form.find('.vh-js-token').val(),
                                visitor_id: v.getVisitorId(),
                                browser_token: browser_token, // Get browser cookie.
                                verifyOtp: verifyOtp // Verify the OTP if 2FA enabled.
                            },
                            success: function (response) {
                                // Process 2FA if enabled.
                                if (response.success //User validated.
                                    && v.twoFactorAuthenticationEnabled //2FA enabled for the site.
                                    && typeof response.data.two_fa_token != "undefined" // 2FA token exists
                                    && response.data.two_fa_token
                                ) {
                                    var twoFaToken  = response.data.two_fa_token;
                                    var expiration_date  = response.data.expiration_date;
                                    var messageToShow  = response.data.messageToShow;
                                    //Set 2FA token cookie.
                                    setVaharaBrowser2FATokenCookie('two_fa_token', twoFaToken, '/', expiration_date);

                                    $(".vh-verify-token-section").find('.vh-verify-token-message').text(messageToShow);
                                    $(".vh-verify-token-section").slideDown();
                                    verifyOtp = response.data.verify_otp; // Needs to verify token if sent to user mobile.

                                } else if (response.success && v.settings.auth_must_verify_browser === 1) {
                                    var user = response.data.user;
                                    var userId = user.id;
                                    var cookie = doesVaharaBrowserValidatedCookieExist('vahara_browser_validated');
                                    if (userId == cookie) {
                                        _doLoginAction(response.data);
                                    } else {
                                        v.callApi('/auth/send-access-code', {
                                            data: {
                                                userId: userId
                                            },
                                            success: function (response) {
                                                if (response.success) {
                                                    window.userResponse = response.data;
                                                    var modalClass = uiItems.accessCodeVerificationModal;
                                                    _showModal(modalClass);
                                                    setTimeout(function () {
                                                        _showMessage('error', 'This is an unrecognized browser, please check your email for an access code to continue');
                                                    },1000);
                                                } else {
                                                    _showMessage('error', response.message);

                                                }
                                            }
                                        });
                                    }
                                } else if (response.success) {
                                    _doLoginAction(response.data);
                                } else {
                                    _showMessage('error', response.message);
                                }
                            }
                        });
                    });
                }

                if (vhModalClass === uiItems.registerModal && _settings.new_user_registration_enabled) {
                    fvInstances.register = _initFormValidation('.vh-js-signup-form', {
                        'vh-js-reg-email': {
                            validators: {
                                emailAddress: {message: 'Valid E-Mail address is required'},
                                notEmpty: {message: 'E-Mail is required'}
                            }
                        },
                        'vh-js-reg-first-name': {
                            validators: {
                                notEmpty: {message: 'First name is required'}
                            }
                        },
                        'vh-js-reg-last-name': {
                            validators: {
                                notEmpty: {message: 'Last name is required'}
                            }
                        },
                        'vh-js-reg-password': {
                            validators: {
                                notEmpty: {message: 'Password is required'},
                            }
                        },
                        'vh-js-confirm-reg-password': {
                            validators: {
                                notEmpty: {message: 'Password confirmation is required'},
                                identical: {
                                    compare: function () {
                                        return $('.vh-js-signup-form').find('[name="vh-js-reg-password"]').val()
                                    },
                                    message: 'The password and its confirmation should be the same'
                                }
                            }
                        }
                    }, function () {
                        var form = $('.vh-js-signup-form');
                        var phone = '';
                        var sponsor = '';
                        var customer_type = '';
                        var affiliateId = vahara.getCookie('affiliate_id');


                        showLoading(true);
                        var emailAddress = form.find('.vh-js-reg-email').val();
                        v.callApi('/auth/register', {
                            data: {
                                email: emailAddress.toLowerCase(),
                                first_name: form.find('.vh-js-reg-first-name').val(),
                                last_name: form.find('.vh-js-reg-last-name').val(),
                                password: form.find('.vh-js-reg-password').val(),
                                confirm_password: form.find('.vh-js-confirm-reg-password').val(),
                                visitor_id: v.getVisitorId(),
                                affiliate_user_id: affiliateId,
                                phone: phone,
                                sponsor_id: sponsor,
                                customer_type: customer_type,
                                has_account: 1
                            },
                            success: function (response) {
                                showLoading(false);
                                if (response.success) {

                                    var user = response.data.user;
                                    v.userId = user.id;
                                    v.userName = user.first_name + ' ' + user.last_name;
                                    v.user = response.data.user;
                                    v.dataToken = response.data.token;

                                    v.triggerEvent('vh-login:registered');
                                    _doLoginAction(response.data);

                                } else {
                                    _showMessage('error', response.message);
                                }
                            }
                        });
                    });
                }

                /* Moved below to use for both the update and register modal*/
                var minPasswordValidation = function () {
                    return {
                        validate: function (input) {
                            var value = input.value;
                            if (value === '') {
                                return {
                                    valid: true,
                                };
                            }

                            // Check the password strength. Password length should be at least 6 characters long
                            if (value.length < 6) {
                                return {
                                    valid: false,
                                    message: 'The password must be more than 6 characters long',
                                };
                            }

                            return {
                                valid: true,
                            };
                        },
                    };
                };

                if (fvInstances.updatePassword) {
                    fvInstances.updatePassword.registerValidator('checkPassword', minPasswordValidation);

                    fvInstances.updatePassword.addField('vh-js-up-password', {
                        selector: '.vh-js-up-password',
                        validators: {
                            checkPassword: {
                                message: 'The password is too weak'
                            },
                        }
                    });
                }

            }
        };

        var _doLogout = function () {
            if (_isLoggedIn) {
                window.vahara.Alert.confirm('Logout', 'Are you sure you want to log out?', {
                    onOk: function () {
                        _doLogoutAction();
                    }
                });
            }
        };

        var doesVaharaBrowserValidatedCookieExist = function (name) {
            var cookieName = name + "=";
            var allCookies = document.cookie.split(';');
            for (var i = 0; i < allCookies.length; i++) {
                var cookie = allCookies[i];
                while (cookie.charAt(0) == ' ') {
                    cookie = cookie.substring(1, cookie.length);
                }
                if (cookie.indexOf(cookieName) == 0) {
                    return cookie.substring(cookieName.length, cookie.length);
                }
            }
            return false;
        };

        var setVaharaBrowserValidatedCookie = function (name, value, path) {
            var expiration_date = new Date();
            expiration_date.setFullYear(expiration_date.getFullYear() + 10);
            expiration_date = expiration_date.toGMTString();

            var cookie_string = escape(name) + "=" + escape(value) +
                "; expires=" + expiration_date;
            if (path != null) {
                cookie_string += "; path=" + path;
            }

            cookie_string += "; SameSite=None;"

            document.cookie = cookie_string;
        };

        var setVaharaBrowser2FATokenCookie = function (name, value, path, expiration_date) {
            expiration_date = new Date(expiration_date);
            expiration_date = expiration_date.toGMTString();
            var cookie_string = escape(name) + "=" + escape(value) + "; expires=" + expiration_date;
            if (path != null) {
                cookie_string += "; path=" + path;
            }
            document.cookie = cookie_string;
        }

        var _doLogoutAction = function () {
            v.callApi('/auth/logout', {
                success: function (response) {
                    if (response.success === true) {
                        v.userId = null;
                        v.userName = '';
                        v.user = null;
                        v.dataToken = null;

                        if (window.vaharaAuthTriggerLocalLogin) {
                            // Call the local site to have it verify that the user
                            // is logged in, and at what level.
                            v.jQuery.ajax('/logout_callback', {
                                data: {token: response.data.token},
                                success: function (data) {
                                    //v.triggerEvent('vh-login:logged-out');
                                    //console.log('local callback response:');
                                    //console.log(data);
                                }
                            });
                        }

                        //console.log('Logged out event called');
                        v.triggerEvent('vh-login:status-updated');
                        v.triggerEvent('vh-login:logged-out');
                    }
                }
            });
        };

        var _singleSignon = function () {
            var url = window.location.href;
            var tokenPos = url.indexOf('?token=');
            if (tokenPos > 0) {
                var token = url.substring(tokenPos + 7);
                v.callApi('/auth/single-signon', {
                    data: {token: token},
                    success: function (response) {
                        if (response.success) {
                            var url = window.location.protocol + '//' + window.location.hostname + '/' + response.data.user.username + '/account-details';
                            v.triggerEvent('vh-login:single-signon-ok');
                            v.onEvent('vh-login:status-updated', function (e) {
                                window.location = url;
                            });
                            setTimeout(function () {
                                window.location = url;
                            }, 5000);
                            _doLoginAction(response.data);
                        } else {
                            v.triggerEvent('vh-login:single-signon-failed');
                        }
                    }
                });
            }
        };

        var _checkForPasswordReset = function () {
            var hash = window.location.hash.replace(/^#!/, '');
            hash = hash.split('=', 2);

            if (hash.length > 1 && hash[0] === 'reset-password' && hash[1].length === 64) {
                _updatePasswordToken = hash[1];
                window.location.hash = '#!home';
            }

            if (hash.length === 1) {
                var modalClass = null;
                switch (hash[0]) {
                    case 'login':
                        modalClass = '.vh-js-login-box';
                        break;
                    case 'reset':
                        modalClass = '.vh-js-reset-password-box';
                        break;
                }

                if (modalClass !== null) {
                    window.location.hash = '#!home';
                    setTimeout(function () {
                        _showModal(modalClass);
                    }, 1000);
                }
            }
        };

        var _bindEvents = function () {
            $(window).on('vh-login:status-updated', function (e) {
                _isLoggedIn = v.userId !== 'undefined' && v.userId !== null && v.userId > 0;
                if (_firstLoad) {
                    _updateLayout();
                    setTimeout(function () {
                        _updateLayout();
                    }, 500);
                    setTimeout(function () {
                        _updateLayout();
                    }, 1000);
                    _firstLoad = false;
                } else {
                    _updateLayout();
                }
            });

            // If there is a vh-ds-trigger-single-signon then trigger that--this is a
            // Direct Scale single signon to this server
            if ($('.vh-js-trigger-single-signon').length) {
                setTimeout(function () {
                    _singleSignon();
                }, 1000);
            }


            $(document).on('click', '[data-vh-js-switch]', function () {
                var showSelector = $(this).data('vh-js-switch');
                _showModal(showSelector);
            });

            // If they click on a login-required link, show the login modal
            // and set up an event to jump to the href they clicked on. Store
            // the href in a global spot so that if they click on, say, 5 login-required
            // it only jumps to the most recent one...

            $(document).on('click', '.vh-js-link-login-required', function (e) {
                if (!_isLoggedIn) {
                    e.preventDefault();

                    _showModal(uiItems.loginModal);

                    // Only set up the jumpto callback once
                    if (!v.jumpToAfterLogin) {
                        v.onEvent('vh-login:logged-in', function () {
                            // Give the browser a little time to notify the local site of the login before redirecting
                            setTimeout(function () {
                                window.location = v.jumpToAfterLogin;
                            }, 1000);
                        });
                    }
                    v.jumpToAfterLogin = $(e.currentTarget).attr('href');
                }
            });

            $(document).on('click', uiItems.loginBtn, function (e) {
                e.preventDefault();
                if (!_isLoggedIn) {
                    if (document.readyState === 'complete') {
                        _showModal(uiItems.loginModal);
                    }
                }
            });

            $(document).on('click', uiItems.loginBtnAlwaysVisible, function (e) {
                e.preventDefault();
                if (document.readyState === 'complete') {

                    _showModal(uiItems.loginModal);
                }
            });

            $(document).on('click', uiItems.loginLogoutBtn, function (e) {
                e.preventDefault();
                if (!_isLoggedIn) {

                    _showModal(uiItems.loginModal);
                } else {
                    _doLogout();
                }
            });

            $(document).on('click', uiItems.logoutBtn, function (e) {
                e.preventDefault();
                if (_isLoggedIn) {
                    $(uiItems.parentModal).modal('hide');
                    _doLogout();
                }
            });

            $(document).on('click', uiItems.logoutBtnAlwaysVisible, function (e) {
                e.preventDefault();
                $(uiItems.parentModal).modal('hide');
                _doLogout();
            });

            $(document).on('click', '.vh-js-show-password', function (e) {
                e.preventDefault();
                var pwel = $('.vh-js-password', $(uiItems.root));
                if (pwel.val().length > 0) {
                    if (pwel.attr('type') === 'text') {
                        pwel.attr('type', 'password');
                        this.text = 'Show password';
                    } else {
                        pwel.attr('type', 'text');
                        this.text = 'Hide password';
                    }
                }
            });

            $(document).on('hide.bs.modal', uiItems.parentModal, function () {
                _resetForms();
            });

            $(window).on('hashchange', function (e) {
                _checkForPasswordReset();
            });

            $(document).on('submit', 'form.vh-js-frm-init-guest-checkout', function (e) {
                e.preventDefault();

                showLoading(true);

                var form = $(this);
                v.callApi('/auth/register-guest', {
                    data: {
                        email: form.find('.vh-js-gc-email').val(),
                        visitor_id: v.getVisitorId(),
                        affiliate_user_id: affiliateId,
                        path: window.location.pathname
                    },
                    success: function (response) {
                        showLoading(false);
                        if (response.success === true) {
                            var redirectTo = null;
                            redirectTo = _settings.store_checkout_page;
                            _doLoginAction(response.data, redirectTo);
                        } else {
                            alert(response.message);
                        }
                    }
                });
            })
        };

        var _resetForms = function () {
            if (fvInstances.login !== null) {
                fvInstances.login.resetForm(true).destroy();
            }
            if (fvInstances.register !== null) {
                fvInstances.register.resetForm(true).destroy();
            }
            if (fvInstances.resetPassword !== null) {
                fvInstances.resetPassword.resetForm(true).destroy();
            }
        };

        var _updateLayout = function () {
            if (!_isLoggedIn) {
                $(uiItems.loginBtn).show();
                $(uiItems.logoutBtn).hide();
                if (_updatePasswordToken !== null) {
                    setTimeout(function () {

                        _showModal(uiItems.updatePasswordModal);
                    }, 1000);
                }
            } else {
                $(uiItems.parentModal).modal('hide');
                $(uiItems.loginBtn).hide();
                $(uiItems.logoutBtn).show();
            }
        };

        var _initFormValidation = function (vhFormClass, fieldValidators, successCallback) {
            return FormValidation.formValidation(
                document.querySelector(vhFormClass), {
                    fields: fieldValidators,
                    plugins: {
                        trigger: new FormValidation.plugins.Trigger({event: 'submit'}),
                        bootstrap: new FormValidation.plugins.Bootstrap(),
                        submitButton: new FormValidation.plugins.SubmitButton(),
                        icon: new FormValidation.plugins.Icon({
                            valid: 'fa fa-check',
                            invalid: 'fa fa-times',
                            validating: 'fa fa-refresh'
                        }),
                    },
                }
            ).on('core.form.valid', function () {
                successCallback();
            });
        };

        var _setRegStatus = function () {
            if (_settings.new_user_registration_enabled === false || _settings.new_user_registration_enabled === null || _settings.new_user_registration_enabled === "0") {
                $('.vh-signup-login-popup-box').find('.vh-js-for-reg-on').remove();
            }
        };

        var _checkAuthStatus = function () {
            // Check login status
            v.callApi('/auth/status', {
                success: function (result) {
                    if (result.success) {
                        var data = result.data;

                        if (data.guest_account) {
                            v.guestUser = true;
                            v.userId = null;
                            v.user = null;
                            v.dataToken = null;
                            return true;
                        }

                        if (data.user_id && data.user_id > 0) {
                            window.vaharaUserId = data.user_id;
                        }

                        // If the orbit site says the user is logged in
                        if (data.user_id && parseInt(data.logged_in) === 1) {

                            var trulyLoggedIn = true;

                            //console.log('local says vahara user id is ' + window.vaharaUserId);
                            if (typeof window.vaharaUserId !== 'undefined') {
                                if (window.vaharaUserId > 0) {
                                    // If the local site has declared that a user is logged in, but
                                    // the orbit user id doesn't match the local user, log the local user out
                                    if (data.user_id != window.vaharaUserId) {
                                        //console.log('user id mismatch');
                                        trulyLoggedIn = false;
                                        if (window.vaharaAuthTriggerLocalLogin) v.jQuery.ajax('/logout_callback');
                                    }

                                    // If the local site has declared that a user isn't logged in, but the user is
                                    // logged in on orbit, log them out on orbit
                                } else {

                                    console.log('window.vaharaUserId is present but not >0, so user not logged in locally, logging out of Vahara.');
                                    trulyLoggedIn = false;
                                    _doLogoutAction();
                                }
                            }

                            // Set the user data in the window if they are logged in
                            if (trulyLoggedIn) {
                                v.userId = data.user_id;
                                v.userName = data.first_name + ' ' + data.last_name;
                            }

                            // If orbit doesn't say the user is logged in
                        } else {
                            // If the local site has declared that a user is logged in, log them out
                            if (typeof window.vaharaUserId !== 'undefined' && window.vaharaUserId > 0) {
                                //console.log('local site says yes logged in, vahara says no, logging out on local');
                                if (window.vaharaAuthTriggerLocalLogin) v.jQuery.ajax('/logout_callback');
                            }
                        }

                    } else if (result.success == false) {
                        // If the local site has declared that a user is logged in, log them out
                        if (typeof window.vaharaUserId !== 'undefined' && window.vaharaUserId > 0) {
                            //console.log('local site says yes logged in, vahara says no, logging out on local');
                            if (window.vaharaAuthTriggerLocalLogin) v.jQuery.ajax('/logout_callback');
                        }
                    }

                    v.triggerEvent('VaharaUpdateLoginStatus');
                    v.triggerEvent('vh-login:status-updated');
                }
            });
        };

        var _processUserLoginByToken = function (token) {
            v.callApi('/auth/auth-reg-login-action', {
                data: {
                    token: token
                },
                success: function (response) {
                    if (response.success) {
                        _doLoginAction(response.data);
                        window.location = '/';
                    }
                }
            });

            v.triggerEvent('vh-login:status-updated');
            v.triggerEvent('vh-login:logged-in');

        };

        /* END PRIVATE FUNCTIONS & VARIABLES */

        /* START PUBLIC FUNCTIONS & VARIABLES */
        var internalInit = function () {
            if (_initialized) {
                throw Error('vAuth already initialized.');
            }

            // These should be loaded in packages or by the local site (bootstrap)
            //v.loadCss('https://use.fontawesome.com/47aa470d29.css');
            //v.loadCss('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css');

            _bindEvents();

            _checkAuthStatus();

            _settings = v.settings;

            if (!_settings.store_checkout_page) {
                _settings.store_checkout_page = 'checkout';
            }

            // Pull in settings from window.vaharaAuthSettings if passed
            if (window.vaharaAuthSettings) {
                for (var i in vaharaAuthSettings) {
                    _settings[i] = vaharaAuthSettings[i];
                }
            }

            var remoteUrl = window.location.href;
            var remoteUrlArr = remoteUrl.split("/#vlt");
            if (typeof remoteUrlArr[1] !== "undefined" && remoteUrlArr[1] !== null && remoteUrlArr[1] !== '') {
                var token = remoteUrlArr[1];
                _processUserLoginByToken(token);
            }

            window.vahara.onEvent('vh-login:logged-in', function (data) {
                if (_settings.no_reload_after_login !== true) {
                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                }
            });

            window.vahara.onEvent('vh-login:show-guest-checkout-options', function (data) {
                var modalClass = '.vh-js-guest-checkout-options-box';
                _showModal(modalClass);
            });

            window.vahara.onEvent('vh-login:show-login', function (data) {
                var modalClass = '.vh-js-login-box';
                _showModal(modalClass);
            });

            window.vahara.onEvent('vh-login:show-registration', function (data) {
                var modalClass = uiItems.registerModal;
                _showModal(modalClass);
            });

            window.vahara.onEvent('vh-login:trigger-silent-logout', function (data) {
                if (v.userId > 0) {
                    _doLogoutAction();
                }
            });

            window.vahara.onEvent('vh-login:logged-out', function (data) {
                if (_settings.no_reload_after_login !== true) {
                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                }
            });

            _checkForPasswordReset();
            _initialized = true;
        };

        /* END PUBLIC FUNCTIONS & VARIABLES */

        /**
         *  Hide/show loader icon
         * @param show
         */
        function showLoading(show) {
            if (typeof show === 'undefined') show = true;
            if (show) {
                $('.vh-js-signup-login-popup-box').addClass('loading');
            } else {
                setTimeout(function () {
                    $('.vh-js-signup-login-popup-box').removeClass('loading')
                }, 300);
            }
        }

        internalInit();
    };

}

function initVaharaAuth() {
    if (!window.vahara.clientJqueryBootstrap4Path) {
        var scriptParts = window.vhAuthCurrentScript.split('/');
        window.vahara.clientJqueryBootstrap4Path = scriptParts.slice(0, scriptParts.length - 2).join('/');
    }

    $$ = window.jQuery;

    var root = $$('body');
    var path = window.vahara.clientJqueryBootstrap4Path;

    if (typeof window.vaharaAuthTriggerLocalLogin === 'undefined' || window.vaharaAuthTriggerLocalLogin === null) {
        window.vaharaAuthTriggerLocalLogin = false;
    }

    if (window.vaharaLibsResourcePath) {
        path = window.vaharaLibsResourcePath;
    }

    if (window.vaharaAuthResourcePath) {
        path = window.vaharaAuthResourcePath;
    } else {
        path += '/auth';
    }
    var template = path + '/auth.tpl';
    var css = path + '/auth.css';

    if (!window.vaharaLibsNoAutoLoadCss && !window.vaharaAuthNoAutoLoadCss) {
        $$('head').append('<link id="client_auth_css" rel="stylesheet" href="' + css + '" type="text/css">');
    }
    if (!window.vaharaLibsNoAutoRun && !window.vaharaAuthNoAutoRun) {
        if (!window.vaharaLibsNoAutoLoadHtml && !window.vaharaAuthNoAutoLoadHtml) {
            $$.get(template, function (data) {
                root.prepend(data);

                (new VaharaAuth($$)).init();
            });
        } else {
            (new VaharaAuth($$)).init();
        }
    }

    window.removeEventListener('load', initVaharaAuth, false);
}

window.vhAuthCurrentScript = document.currentScript.src;
window.addEventListener('load', initVaharaAuth, false);
