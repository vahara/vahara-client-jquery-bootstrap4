<div class="vh-client-component-auth">
    <div class="vh-signup-login-popup-box vh-js-signup-login-popup-box">
        <div class="vh-overlay vh-js-overlay">
            <img src="https://o2fdv.vahara.com/img/animated_spinner.gif">
        </div>
        <div class="vh-js-authreg-modal modal fade" role="dialog">
            <div class="modal-dialog shadow">
                <div class="modal-content">
                    <!--login start-->
                    <div class="vh-js-login-box vh-login-box vh-js-page-modal-box">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <h2 class="mb-4">Log in to continue</h2>
                            <div class="vh-success vh-js-success"></div>
                            <div class="vh-errors vh-js-errors"></div>
                            <form class="vh-js-login-form" method="post">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control vh-js-username" name="vh-js-username" placeholder="E-Mail">
                                        <div class="input-group-append input-icon">
                                            <i class="fa fa-envelope"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="password" class="form-control vh-js-password" name="vh-js-password" placeholder="Password">
                                        <div class="input-group-append input-icon">
                                            <i class="fa fa-unlock-alt"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-12 mb-3">
                                        <div class="pull-right">
                                            <a class="vh-js-show-password" href="javascript:void(0);">Show password</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group vh-verify-token-section">
                                    <div class="input-group">
                                        <input type="text" class="form-control vh-js-token" name="vh-js-token" placeholder="Enter OTP">
                                        <div class="input-group-append input-icon">
                                            <i class="fa fa-mobile"></i>
                                        </div>
                                    </div>
                                    <p class="vh-verify-token-message"></p>
                                </div>
                                <button type="submit" class="btn btn-primary btn-block btn-process-login">Login</button>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <p><a href="javascript:void(0);" data-vh-js-switch=".vh-js-reset-password-box" class="vh-auth-reg-forgot-password-link">Forgot password?</a></p>
                            <!--
                                                        <hr class="vh-js-for-reg-on vh-auth-reg-sign-up-hr">
                                                        <p class="vh-js-for-reg-on vh-auth-reg-no-account"><span>Don't have an account?</span> <a href="#!signup" data-vh-js-switch=".vh-js-page-signup-box">Sign up</a></p>
                            -->
                        </div>
                    </div>
                    <!--login end-->

                    <!--signup-form start-->
                    <div class="vh-js-page-signup-box vh-signup-form-box vh-js-page-modal-box vh-js-for-reg-on">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <div class="vh-success vh-js-success"></div>
                            <div class="vh-errors vh-js-errors"></div>
                            <form class="vh-js-signup-form" method="post">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control vh-js-reg-email" name="vh-js-reg-email" placeholder="Email address">
                                        <div class="input-group-append input-icon">
                                            <i class="fa fa-envelope"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control vh-js-reg-first-name" name="vh-js-reg-first-name" placeholder="First name">
                                        <div class="input-group-append input-icon">
                                            <i class="fa fa-user"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control vh-js-reg-last-name" name="vh-js-reg-last-name" placeholder="Last name">
                                        <div class="input-group-append input-icon">
                                            <i class="fa fa-user"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="password" class="form-control vh-js-reg-password" name="vh-js-reg-password" placeholder="Create a Password">
                                        <div class="input-group-append input-icon">
                                            <i class="fa fa-unlock-alt"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="password" class="form-control vh-js-confirm-reg-password" name="vh-js-confirm-reg-password" placeholder="Password confirmation">
                                        <div class="input-group-append input-icon">
                                            <i class="fa fa-unlock-alt"></i>
                                        </div>
                                    </div>
                                </div>
                                <!-- Add the field if DS project-->
                                <div class="vh-js-ds-fields"></div>

                                <div class="birthday-box">
                                    <p>We'll send you marketing promotions, special offers, inspiration, and policy updates via email.</p>
                                </div>
                                <button type="submit" class="btn btn-default email-form-signup-butt b-bttu">Sign up</button>

                                <!-- Add the DS Sponsor field if DS project-->
                                <div class="vh-js-ds-sponsor-field"></div>

                                <div class="dont-what">
                                    <input name="" type="checkbox" value="">I don't want to receive marketing messages. I can also opt out of these at any time in my account settings or via the link in the message.
                                </div>
                                <div class="or-box">
                                    <div class="line-box"></div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <p>Already have an account? <a href="javascript:void(0);" data-vh-js-switch=".vh-js-login-box">Log in</a></p>
                        </div>
                    </div>
                    <!--signup-form end-->

                    <!--reset-password start-->
                    <div class="vh-js-reset-password-box vh-reset-password-box vh-js-page-modal-box">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <form class="vh-js-reset-password-form" method="post">
                            <div class="modal-body">
                                <h2 class="mb-4">Reset Password</h2>
                                <p>Enter the email address associated with your account, and we’ll email you a link to reset your password.</p><br>
                                <div class="vh-success vh-js-success"></div>
                                <div class="vh-errors vh-js-errors"></div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control vh-js-reset-email" name="vh-js-reset-email" placeholder="Your e-mail address">
                                        <div class="input-group-append input-icon">
                                            <i class="fa fa-envelope"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="row">
                                    <div class="col-lg-7 mb-7">
                                        <div class="another-account-box">
                                            <a href="javascript:void(0);" data-vh-js-switch=".vh-js-login-box">⟵ Back to Login</a>
                                        </div>
                                    </div>
                                    <div class="col-lg-5 mb-5">
                                        <button type="submit" class="btn btn-primary btn-block">Send Reset Link</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!--reset-password end-->

                    <!-- update password start-->
                    <div class="vh-js-update-password-box vh-update-password-box vh-js-page-modal-box">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <h2 class="mb-4">Update your password</h2>
                            <div class="vh-success vh-js-success"></div>
                            <div class="vh-errors vh-js-errors"></div>
                            <form class="vh-js-update-password-form" method="post">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="password" class="form-control vh-js-up-password" name="vh-js-up-password" placeholder="Password">
                                        <div class="input-group-append input-icon">
                                            <i class="fa fa-unlock-alt"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="password" class="form-control vh-js-up-password-confirmation" name="vh-js-up-password-confirmation" placeholder="Password">
                                        <div class="input-group-append input-icon">
                                            <i class="fa fa-unlock-alt"></i>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary btn-block">Update</button>
                            </form>
                        </div>
                    </div>
                    <!-- update password end-->

                    <!--guest checkout options box start-->
                    <div class="vh-js-guest-checkout-options-box vh-guest-checkout-options-box vh-js-page-modal-box">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <h2 class="mb-4">Please select an option to continue:</h2>
                            <div class="mb-5">
                                <a class="vh-g-btn btn btn-primary btn-block" href="javascript:void(0);" data-vh-js-switch=".vh-js-guest-checkout-email-box">Guest Checkout</a>
                                <a class="vh-g-btn btn btn-primary btn-block" href="javascript:void(0);" data-vh-js-switch=".vh-js-login-box">Login</a>
                                <a class="vh-g-btn btn btn-primary btn-block" href="javascript:void(0);" data-vh-js-switch=".vh-js-page-signup-box">Sign up</a>
                            </div>
                        </div>
                    </div>
                    <!--guest checkout options box end-->

                    <!--guest checkout email box start-->
                    <div class="vh-js-guest-checkout-email-box vh-guest-checkout-email-box vh-js-page-modal-box">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <h4 class="mb-4">Please enter your email address:</h4>
                            <div class="mb-5">
                                <form method="post" class="vh-js-frm-init-guest-checkout">
                                    <input required type="email" class="form-control vh-g-input vh-js-gc-email mb-3" name="email" placeholder="E-Mail" autocomplete="off">
                                    <button class="vh-g-btn btn btn-primary" type="submit">Continue</button>
                                    <button class="vh-g-btn btn btn-primary" type="button" data-vh-js-switch=".vh-js-guest-checkout-options-box">Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--guest checkout email box end-->
                </div>
            </div>
        </div>
    </div>

    <script class="vh-js-template-ds-phone-field" data-priority="100" type="text/x-handlebars-template">
        <div class="form-group">
            <div class="input-group">
                <input type="tel" class="form-control vh-js-reg-phone" name="vh-js-reg-phone" placeholder="Phone Number">
                <div class="input-group-append input-icon">
                    <i class="fa fa-phone"></i>
                </div>
            </div>
        </div>
    </script>

    <!-- SPONSOR SCRIPT -->
    <script class="vh-js-template-ds-sponsor-field" data-priority="100" type="text/x-handlebars-template">
        <div class="form-group">
            <label>Recommended by a Pearl Stylist?</label>
            <div class="input-group">
                <select name="sponsor" class="form-control vh-js-reg-sponsor" style="height: 50px;">
                    <option value="" disabled selected hidden>Select {{representative_term}}</option>
                    <option value="{{default_customer_sponsor_id}}"> I don't have a {{representative_term}}</option>
                    {{#each sponsors}}
                    <option value="{{id}}" >{{first_name}} {{last_name}}</option>
                    {{/each}}
                </select>
            </div>
        </div>
    </script>

    <script class="vh-js-template-ds-fields" data-priority="100" type="text/x-handlebars-template">
        <div class="form-group">
            <div class="input-group">
                <input type="tel" class="form-control vh-js-reg-phone" name="vh-js-reg-phone" placeholder="Phone Number">
                <div class="input-group-append input-icon">
                    <i class="fa fa-phone"></i>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="input-group">
                {{#if hasSponsor}}
                {{#if replicated_site_can_select_sponsor}}
                <select name="sponsor" class="form-control vh-js-reg-sponsor" style="height: 50px;">
                    <option value="" disabled selected hidden>Select {{representative_term}}</option>
                    <option value="{{default_customer_sponsor_id}}"> I don't have a {{representative_term}}</option>
                    {{#each sponsors}}
                    <option value="{{id}}" >{{first_name}} {{last_name}}</option>
                    {{/each}}
                </select>
                {{else}}
                <p class="vh-default-sponsor"><span class="font-weight-bold">Your sponsor is</span>: {{sponsorName}}</p>
                <input type="hidden" class="vh-js-reg-sponsor" value="{{selectedSponsorId}}">
                {{/if}}
                {{else}}
                <select name="sponsor" class="form-control vh-js-reg-sponsor" style="height: 50px;">
                    <option value="" disabled selected hidden>Select {{representative_term}}</option>
                    <option value="{{default_customer_sponsor_id}}"> I don't have a {{representative_term}}</option>
                    {{#each sponsors}}
                    <option value="{{id}}" >{{first_name}} {{last_name}}</option>
                    {{/each}}
                </select>
                {{/if}}
            </div>
        </div>
    </script>
</div>
