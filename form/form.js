function VaharaForms($) {

    /** EXPOSED CLASS **/
    this.init = function () {
        //console.log('VaharaForms Initialized');

        var _settings = null;

        /* START PRIVATE FUNCTIONS & VARIABLES */
        var _initialized = false;
        var v = window.vahara;


        /* START PRIVATE FUNCTIONS & VARIABLES */

        var _bindEvents = function() {
            var $element = $('.vh-js-item-form');

            $('.vh-js-item-form .tooltip-element').each(function () {
                $(this).attr('data-toggle','tooltip')
                    .attr('data-type','tooltip-primary')
                    .attr('data-original-title',$(this).attr('tooltip'));
            });

            $('.vh-js-item-form [data-toggle="tooltip"]').each(function () {

                var toolTipOptions = {
                    boundary: 'body',
                    html: true,
                    trigger: 'hover',
                    placement: 'top'
                };

                if ($(this)[0].hasAttribute('data-type')) {
                    toolTipOptions['template'] =
                        '<div class="tooltip ' + $(this).attr('data-type') + '" role="tooltip"     >' +
                        '   <div class="arrow"></div>' +
                        '   <div class="tooltip-inner"></div>' +
                        '</div>';

                    $(this).tooltip(toolTipOptions);
                }
            });


            $(".vh-js-item-form").find("input[type=checkbox]").on("change", function () {
                var checkBoxName = $(this).attr('name');
                var checkBoxList = $("input[name='" + checkBoxName + "']");
                var checkedCheckBoxList = $("input[name='" + checkBoxName + "']:checked");
                if (checkedCheckBoxList.length > 0) {
                    removeRequiredToCheckBoxList(checkBoxList);
                } else {
                    addRequiredToCheckBoxList(checkBoxList);
                }
            });

            if ($element.length) {
                $(document).on('submit', '.vh-js-item-form', function (e) {
                    e.preventDefault();    //stop form from submitting

                    var $form = $(this);
                    var $submitButton = $form.find("button[type='submit'], input[type='submit']");

                    // Disable submit button to prevent duplicate submissions
                    window.vhFormSubmitButton = $submitButton;
                    window.vhFormSubmitOriginalText = $submitButton.text();
                    $submitButton.prop("disabled", true).text("Submitting...");
                    console.log('disable button');

                    var formDataArray = new FormData();
                    //Form data
                    var form_data = $(this).serializeArray();

                    // File data, add to form data to pass the server
                    var file_input = $('input[type="file"]');
                   for (var i = 0; i < file_input.length; i++) {
                     var file_data = file_input[i].files;
                       var name = $(file_input[i]).attr('name');
                       for (var j = 0; j < file_data.length; j++) {
                         formDataArray.append(name, file_data[j]);
                      }
                    }
                    var $form = $(this);

                    /*Get all the checkbox list*/
                    var checkBoxNameArr = {};
                    var checkboxes = $(this).find("input[type=checkbox]");
                    $.each(checkboxes, function () {
                        checkBoxNameArr[$(this).attr('name')] = $(this).attr('name');
                    });

                    var radioGroupNameArr = {};
                    var radioButtons = $(this).find("input[type=radio]");
                    $.each(radioButtons, function () {
                        radioGroupNameArr[$(this).attr('name')] = $(this).attr('name');
                    });

                    var interactionId = $(this).attr('data-vh-item-form-interaction-id');
                    var elementId = $(this).closest("[data-element-id]").data("element-id");

                    var self = this;
                    var dataArray = $(self).serializeArray();
                    var data = {};

                    var checkBoxArr = new Array();

                    $.each(checkBoxNameArr, function (checkBoxName, value) {
                        var checkBoxList = $("input[name='" + checkBoxName + "']");
                        $.each(checkBoxList, function () {
                            var elementName = $(this).attr('name');
                            var item = {};
                            if ($(this).prop("checked") === true) {
                                var checkBoxId = $(this).attr('id');
                                var otherTextId = checkBoxId + '-' + 'value';
                                var otherTextElement = $("#" + otherTextId);
                                var checkElement = $("#" + checkBoxId);
                                if (otherTextElement.length > 0) {
                                    item[elementName] = 'other: ' + otherTextElement.val();
                                } else {
                                    item[elementName] = checkElement.val();
                                }
                                checkBoxArr.push(item);
                            }
                        });
                    });

                    $.each(dataArray, function (key, value) {
                        formDataArray.append(value.name, value.value);
                        data[value.name] = value.value;
                    });

                    var extractedCheckBoxValues = {};
                    $.each(checkBoxArr, function (key, value) {
                        if (typeof value != "undefined" && typeof value != null && typeof Object.entries(value) != "undefined" && typeof Object.entries(value)[0] != "undefined" && typeof Object.entries(value)[0][0] != "undefined") {
                            var objectKeyArray = Object.entries(value)[0][0];
                            var objectKey = objectKeyArray.replace('[]', '');

                            if (typeof extractedCheckBoxValues[objectKeyArray] != "undefined" && typeof data[objectKeyArray] != null) {
                                var existingData = extractedCheckBoxValues[objectKeyArray];
                                extractedCheckBoxValues[objectKeyArray] = existingData + ', ' + value[objectKeyArray];
                            } else {
                                extractedCheckBoxValues[objectKeyArray] = value[objectKeyArray];
                            }

                            delete data[objectKeyArray];
                            data[objectKey] = extractedCheckBoxValues[objectKeyArray];
                            formDataArray.append(objectKey, 'other: ' + extractedCheckBoxValues[objectKeyArray]);
                        }
                    });

                    $.each(radioGroupNameArr, function (radioName, value) {
                        var radioCheckedItem = $("input[name='" + radioName + "']:checked");
                        var radioId = radioCheckedItem.attr('id');
                        var otherTextId = radioId + '-' + 'value';
                        var otherTextElement = $("#" + otherTextId);
                        if (otherTextElement.length > 0) {
                            data[radioName] = 'other: ' + otherTextElement.val();
                            formDataArray.append(radioName, 'other: ' + otherTextElement.val());
                        } else {
                            data[radioName] = radioCheckedItem.val();
                            formDataArray.append(radioName, radioCheckedItem.val());
                        }
                    });

                    formDataArray.append('interactionId', interactionId);
                    formDataArray.append('elementId', elementId);
                    formDataArray.append('page_link', window.location.href);

                    ItemFormSubmit(self, formDataArray);
                });
            }
        };


        var ItemFormSubmit = function (self, data) {
            var checkboxes = $(self).find("input[type=checkbox]");
            var checkBoxName = $(checkboxes).attr('name');
            var checkBoxList = $("input[name='" + checkBoxName + "']");

            var referenceSearch = window.location.search.match(/referenceId=(\d+)/);
            if (referenceSearch && typeof referenceSearch[1] !== 'undefined') {
              data.append('referenceId', referenceSearch[1]);
            }

            v.callApi('/item-form/form-submit', {
                data: data,
                success: function (results) {

                    // Re-enable button
                    console.log('re-enable button');
                    window.vhFormSubmitButton.prop("disabled", false).text(window.vhFormSubmitOriginalText);

                    if (results.success && results.data.interaction_complete) {
                        var rd = results.data;

                        $(self).trigger('reset');
                        addRequiredToCheckBoxList(checkBoxList);

                        if (rd.responseMessage) {
                            if (rd.responseMessage.match(/redirect:/)) {
                                var url = rd.responseMessage.replace(/redirect:/, '');
                                window.location = url;
                            } else {
                                window.vahara.Alert.alert('Success', rd.responseMessage);
                            }
                        }
                    } else if (!results.success && results.data.error) {
                        var rd = results.data;

                        if (v.settings && v.settings.captcha_enabled && (v.settings.captcha_enabled == 'yes' || v.settings.captcha_enabled == 1) && results.message == 'Invalid Captcha value') {
                            window.vahara.Alert.alert('Error', 'Please enter the text shown on the image to verify you are a human.');
                            if ($(self).find('.captch-input-field').length) {
                                $(self).find('.captch-input-field').html(rd.img_field);
                            }
                        } else {
                            window.vahara.Alert.alert('Error', rd.error);

                        }
                    }
                },
		error: function() {
		  // Re-enable button
		  console.log('re-enable button');
		  window.vhFormSubmitButton.prop("disabled", false).text(window.vhFormSubmitOriginalText);
		}
            });
        };

        if (v.settings && v.settings.captcha_enabled && (v.settings.captcha_enabled == 'yes'  || v.settings.captcha_enabled == 1)) {
            var fetchCaptchaInput = function (self) {
                var elementId = self.parents('.vhc-js-element').attr('data-element-id');
                v.callApi('/item-form/get-captcha', {
                    data: { elementId: elementId },
                    success: function (results) {
                        if (results.success && results.data.img_field) {
                            var rd = results.data;
                            if (self.find('.captch-input-field').length) {
                                self.find('.captch-input-field').html(rd.img_field);
                            }
                        } else if (!results.success && results.data.error) {
                            window.vahara.Alert.alert('Error', 'Unable to fetch captcha, Please reload the page');
                        }
                    }
                });
            };

            $('.vh-js-item-form').each(function () {
                var self = $(this);
                if ($(this).find('.captch-input-field').length == 0) {
                    // append only before the submit button, there will be chances of multiple buttons
                    $(this).find('.fb-button :submit').last().before('<div class="captch-input-field"></div>');
                    fetchCaptchaInput(self);
                }
            });

            $(document).on('click', '.btn-refresh-v-captcha', function (e) {
                e.preventDefault();    //stop form from submitting
                var $form = $(this).parents('.vh-js-item-form');
                fetchCaptchaInput($form);
            });

        }


        function addRequiredToCheckBoxList(checkBoxList) {
            $.each(checkBoxList, function ($index, $element) {
                if ($($element).attr('aria-required') === 'true') {
                    $($element).attr('required', 'required');
                    if ($($element).hasClass('other-option')) {
                        addRemoveRequireAttributeToOtherField($element);
                    }
                }
            });
        }

        function removeRequiredToCheckBoxList(checkBoxList) {
            $.each(checkBoxList, function ($index, $element) {
                if ($($element).attr('aria-required') === 'true') {
                    $($element).removeAttr('required');
                    var checkBoxId = $($element).attr('id');
                    $("#" + checkBoxId).oninput = this.setCustomValidity('');
                    if ($($element).hasClass('other-option')) {
                        addRemoveRequireAttributeToOtherField($element);
                    }
                }
            });
        }

        function addRemoveRequireAttributeToOtherField($element) {
            var otherCheckId = $($element).attr('id');
            var otherTextId = otherCheckId + '-' + 'value';
            var textElement = $("#" + otherTextId);
            if ($($element).prop("checked") === true) {
                textElement.attr('required', 'required');
            } else {
                textElement.removeAttr('required');
            }
        }

        function updateDefaultRequiredCheckBoxMessage() {
            var checkboxes = $('.vh-js-item-form').find("input[type=checkbox]");
            var checkBoxName = $(checkboxes).attr('name');
            var checkBoxList = $("input[name='" + checkBoxName + "']");
            $.each(checkBoxList, function ($index, $element) {
                if ($($element).attr('aria-required') === 'true') {
                    var checkBoxId = $($element).attr('id');
                    $("#" + checkBoxId).oninput = this.setCustomValidity('');
                    $("#" + checkBoxId).oninvalid = this.setCustomValidity('Please check at least one box to proceed');
                }
            });
        }

        /* END PRIVATE FUNCTIONS & VARIABLES */

        /* START PUBLIC FUNCTIONS & VARIABLES */
        var internalInit = function () {
            if (_initialized) {
                throw Error('VaharaForms already initialized.');
            }

            // These should be loaded in packages or by the local site (bootstrap)
            //v.loadCss('https://use.fontawesome.com/47aa470d29.css');
            //v.loadCss('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css');

            updateDefaultRequiredCheckBoxMessage();

            _bindEvents();

            _settings = v.settings;

            _initialized = true;
        };

        /* END PUBLIC FUNCTIONS & VARIABLES */


        internalInit();
    };

}

function initVaharaForms() {
    $$ = window.jQuery;

    var root = $$('body');
    var path = window.vaharaClientJqueryBootstrap4AuthPath;

    if (window.vaharaLibsResourcePath) {
        path = window.vaharaLibsResourcePath;
    }

    if (window.vaharaAuthResourcePath) {
        path = window.vaharaAuthResourcePath;
    } else {
        path += '/forms';
    }
    var css = path + '/forms.css';

    if (!window.vaharaLibsNoAutoLoadCss && !window.vaharaFormsNoAutoLoadCss) {
        $$('head').append('<link id="client_auth_css" rel="stylesheet" href="' + css + '" type="text/css">');
    }
    if (!window.vaharaLibsNoAutoRun && !window.vaharaFormsNoAutoRun) {
        (new VaharaForms($$)).init();
    }

    window.removeEventListener('load', initVaharaAuth, false);
}

window.vaharaClientJqueryBootstrap4AuthPath = window.vahara.getCurrentScriptPath(-2);

window.addEventListener('load', initVaharaForms, false);
