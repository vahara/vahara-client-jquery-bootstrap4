<div class="vh-client-component-checkout">

    <div class="vh-component-checkout">

        <div class="vh-overlay vh-js-overlay">
            <img src="https://o2fdv.vahara.com/img/animated_spinner.gif">
        </div>

        <!-- container fluid start -->
        <div class="container">
            <div class="row section-top">
                <div class="col-md-12">
                    <!-- START STEP SWITCHERS -->
                    <div class="shop-tab-section">
                        <div class="stepwizard">
                            <div class="setup-panel">
                                <ul class="bootstrapWizard form-wizard clearfix">
                                    <li data-step="shipping">
                                        <a href="javascript:void(0);" type="button" class="btn btn-circle btn-default vh-step-indicator vh-js-step-switch vh-step-active" data-step="shipping_address,shipping_details">
                                            <span class="step">1</span>
                                            <span class="title">Shipping Details</span>
                                        </a>
                                    </li>
                                    <li data-step="payment">
                                        <a href="javascript:void(0);" type="button" class="btn btn-default btn-circle vh-step-indicator vh-js-step-switch" disabled="disabled" data-step="select_card,payment">
                                            <span class="step">2</span>
                                            <span class="title">Payment Details</span>
                                        </a>
                                    </li>
                                    <li data-step="review">
                                        <a href="javascript:void(0);" type="button" class="btn btn-default btn-circle vh-step-indicator vh-js-step-switch" disabled="disabled" data-step="review">
                                            <span class="step">3</span>
                                            <span class="title">Review and Confirm</span>
                                        </a>
                                    </li>
                                    <li data-step="order_complete">
                                        <a href="javascript:void(0);" type="button" class="btn btn-default btn-circle vh-step-indicator vh-js-step-switch" disabled="disabled" data-step="order_complete">
                                            <span class="step">4</span>
                                            <span class="title">Order Complete</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- END STEP SWITCHERS -->
                </div>
            </div>
        </div>
        <!-- container fluid end -->

        <div class="clearfix"></div>

        <!--container-fluid start-->
        <div class="container">
            <div class="col-lg-12 mb-12">

                <!-- STEP 1: SHIPPING -->
                <div class="vh-step-content vh-js-step" data-step="shipping_address">
                    <div class="row">
                        <div class="col-lg-8 mb-8 section-top">
                            <h3>Shipping Address</h3>
                            <form class="vh-js-checkout-shipping-address-form" method="post">
                                <div class="vh-js-shipping-form"></div>
                            </form>
                        </div>
                        <div class="col-lg-4 mb-4">
                            <div class="vh-js-sidebox"></div>
                        </div>
                    </div>
                </div>
                <!-- /STEP 1: SHIPPING -->

                <!-- STEP 2: SHIPPING METHOD -->
                <div class="vh-step-content vh-js-step" data-step="shipping_details">
                    <div class="row">
                        <div class="col-lg-8 mb-8 section-top">
                            <h3>Shipping Details</h3>
                            <form class="vh-js-checkout-shipping-details-form" method="post">
                                <div class="vh-js-shipping-details-form"></div>
                            </form>
                        </div>
                        <div class="col-lg-4 mb-4">
                            <div class="vh-js-sidebox"></div>
                        </div>
                    </div>
                </div>
                <!-- /STEP 2: SHIPPING METHOD -->

                <!-- STEP 2: SELECT PAYMENT -->
                <div class="vh-step-content vh-js-step" data-step="select_card">
                    <div class="row">
                        <div class="col-lg-8 mb-8 section-top">
                            <h3>Choose Saved Payment</h3>
                            <form class="vh-js-select-card-form" method="post">
                                <div class="vh-js-payment-select-container"></div>
                            </form>
                        </div>
                        <div class="col-lg-4 mb-4">
                            <div class="vh-js-sidebox"></div>
                        </div>
                    </div>
                </div>
                <!-- /STEP 2: SELECT PAYMENT -->

                <!-- STEP 2: PAYMENT -->
                <div class="vh-step-content vh-js-step" data-step="payment">
                    <div class="row">
                        <div class="col-lg-8 mb-8 section-top">
                            <h3>Payment Details</h3>
                            <div class="vh-js-payment-container" data-platform="paypal">
                                <form class="vh-js-paypal-payment-form" method="post">
                                    <div class="vh-js-paypal-container"></div>
                                </form>
                            </div>
                            <div class="vh-js-payment-container" data-platform="stripe">
                                <form class="vh-js-stripe-payment-form" method="post">
                                    <div class="vh-js-stripe-container"></div>
                                </form>
                            </div>
                            <div class="vh-js-payment-container" data-platform="nexiopay">
                                <div class="vh-js-nexiopay-container"></div>
                                <form class="vh-js-nexiopay-form" method="post" style="display:none;">
                                    <button type="submit" class="btn btn-primary btn-block btn-lg vh-submit-form">Continue</button>
                                </form>
                            </div>
                            <div class="vh-js-payment-container" data-platform="cms"></div>
                        </div>
                        <div class="col-lg-4 mb-4">
                            <div class="vh-js-sidebox"></div>
                        </div>
                    </div>
                </div>
                <!-- /STEP 2: PAYMENT -->

                <!-- STEP 2: REVIEW -->
                <div class="vh-step-content vh-js-step" data-step="review">
                    <div class="row">
                        <div class="col-lg-8 mb-8 section-top">
                            <h3>Review Order Details</h3>
                            <form class="vh-js-checkout-review-form" method="post">
                                <div class="vh-js-review-form"></div>
                            </form>
                        </div>
                        <div class="col-lg-4 mb-4">
                            <div class="vh-js-sidebox"></div>
                        </div>
                    </div>
                </div>
                <!-- /STEP 2: REVIEW -->

                <!-- STEP 4: ORDER COMPLETE -->
                <div class="vh-step-content vh-js-step" data-step="order_complete">
                    <div class="row">
                        <div class="col-lg-12 mb-8 section-top text-center">
                            <h3>Order Complete</h3>
                            <div class="vh-js-checkout-order-complete"></div>
                        </div>
                    </div>
                </div>
                <!-- /STEP 4: ORDER COMPLETE -->

            </div>
        </div>
        <!--container-fluid end-->

        <div class="vh-payment-modal vh-js-payment-modal">
            <div class="vh-js-modal-body"></div>
        </div>

        <!-- Payment Modal -->
        <div class="modal fade vh-js-payment-modal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="vh-js-payment-frame"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Payment Modal End -->

        <!-- Multipurpose Shipping Modal -->
        <div class="modal fade vh-js-shipping-mp-modal vh-shipping-mp-modal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <button type="button" class="close vh-close-modal" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="vh-js-shipping-mp-wrapper vh-shipping-mp-wrapper"></div>
                </div>
            </div>
        </div>
        <!-- Multipurpose Shipping Modal End -->

    </div>

    <!-- TEMPLATES -->
    <!---------------------------
        Order Details

        Available Variables
        [
            subtotal
            tax
            total
            billing
            [
                first_name
                last_name
                address_1
                address_2
                city
                state
                zip
                country
                phone
            ]
            shipping
            [
                rate
                method_code
                method_label

                first_name
                last_name
                address_1
                address_2
                city
                state
                zip
                country
                phone
            ]
            payment
            [
                transaction_id,
                masked_card
                [
                    expiration
                    lastFour
                    type
                ]
            ]
            line_items
            [
                id
                cart_label
                discount
                image
                name
                price
                quantity
                sku
                total_price
                volume
            ]
        ]
    ----------------------------->
    <script class="vh-js-template-checkout-orderdetails" data-priority="100" type="text/x-handlebars-template">
        <div class="row" style="text-align:left!important">
            <div class="col-md-4 mb-4">
                <h5>Order ID</h5>
                <div class="vh-order-id">
                    {{order_id}}
                </div>
            </div>
        </div>
        <div class="row" style="text-align:left!important">
            {{#if should_ship}}
                <div class="col-lg-4 mb-6">
                    <h5>Shipping Address</h5>
                    <div class="vh-js-shipto-address vh-shipto-address">
                        {{> address shipping}}
                    </div>
                </div>
            {{/if}}
            {{#if billing}}
                <div class="col-lg-4 mb-6">
                    <h5>Billing Address</h5>
                    <div class="vh-js-billto-address vh-billto-address">
                        {{> address billing}}
                    </div>
                </div>
            {{/if}}
            <div class="col-lg-4 mb-6">
                <h5>Payment</h5>
                {{#ifCond payment.method_code 'nexiopay'}}
                    <div class="method-box-1">
                        {{payment.masked_card.type}} <code>xxxx{{payment.masked_card.lastFour}}</code> <br>
                        expires on <code>{{payment.masked_card.expiration}}</code>
                    </div>
                {{/ifCond}}
                {{#ifCond payment.method_code 'stripe'}}
                    <div class="method-box-1">
                        {{payment.masked_card.type}} <code>xxxx{{payment.masked_card.lastFour}}</code> <br>
                        expires on <code>{{payment.masked_card.expiration}}</code>
                    </div>
                {{/ifCond}}

                <div class="method-box-2 mt-4">
                    Transaction ID: <span style="overflow-wrap:break-word;">{{payment.transaction_id}}</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 mt-4">
                <table class="table table-bordered vh-order-details-table">
                    <thead>
                        <tr>
                            <th>Sr#</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Qty.</th>
                            <th>Unit Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{#each line_items}}
                        <tr>
                            <td>{{counter @index}}</td>
                            <td>
                                <img src="{{{image}}}" class="rounded float-left" style="width:100px">
                                <span class="vh-order-product-title ml-4">{{name}}</span>
                            </td>
                            <td class="vh-currency">
                                <span class="vh-order-product-price">${{currency price}}</span><br>
                            </td>
                            <td>
                                <h6 class="vh-order-product-quantity">{{quantity}}</h6>
                            </td>
                            <td class="vh-currency">
                                <span class="vh-order-product-unittotal">${{currency total_price}}</span>
                            </td>
                        </tr>
                        {{/each}}
                        <tr>
                            <td colspan="4">Sub-total</td>
                            <td class="vh-currency">
                                <span class="vh-order-product-subtotal">${{currency subtotal}}</span>
                            </td>
                        </tr>
                        {{#if should_ship}}
                        <tr>
                            <td colspan="4">Shipping & Handling - {{shipping.method_label}}</td>
                            <td class="vh-currency">
                                <span class="vh-order-product-subtotal">${{currency shipping.rate}}</span>
                            </td>
                        </tr>
                        {{/if}}
                        {{#if discount}}
                            <tr>
                            <td colspan="4">Discount</td>
                            <td class="vh-currency">
                                <span class="vh-order-discount-total">${{currency discount}}</span>
                            </td>
                        </tr>
                        {{/if}}

                        {{#if total_with_discount}}
                        <tr>
                            <td colspan="4">Total</td>
                            <td class="vh-currency">
                                <span class="vh-order-product-subtotal">${{currency total_with_discount}}</span>
                            </td>
                        </tr>
                        {{else}}
                        <tr>
                            <td colspan="4">Total</td>
                            <td class="vh-currency">
                                <span class="vh-order-product-subtotal">${{currency total}}</span>
                            </td>
                        </tr>
                        {{/if}}

                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{#if shop_page}}
                    <a href="/{{shop_page}}" class="btn btn-primary">Return to Shopping</a>
                {{else}}
                    <a href="/" class="btn btn-primary">continue</a>
                {{/if}}
            </div>
        </div>
    </script>

    <!---------------------------
        Selct Saved Payment
    ----------------------------->
    <script class="vh-js-template-checkout-select-card" data-priority="100" type="text/x-handlebars-template">
        <div class="form-row">
            <div class="col-md-12">
                <div class="form-group cards mb-5">
                    {{#each cards}}
                        <div class="saved_card mb-3">
                            <label for="card_{{id}}">
                                <input class="chkPaymentCard" id="card_{{id}}" {{#if primary_payment}} checked {{/if}} type="radio" name="payment_card" value="{{id}}"> <strong>{{title_case card_type}}</strong> <code>xxxx{{last_four}}</code> expires <code>{{expiration_month}}/{{expiration_year}}</code>
                            </label>
                        </div>
                    {{/each}}
                    <div class="new_card">
                        <label for="new_card">
                            <input class="chkPaymentCard" id="new_card" type="radio" name="payment_card" value="NEW"> <strong>Add New</strong>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <button class="btn btn-primary btn-block btn-lg vh-submit-form" type="submit">Continue</button>
        </div>
    </script>

    <!---------------------------
        Review Form
    ----------------------------->
    <script class="vh-js-template-checkout-reviewform" data-priority="100" type="text/x-handlebars-template">
        <div class="form-row">
            {{#if should_ship}}
            <div class="col-lg-6 mb-6">
                {{#if should_ship}}
                    <div class="review-details">
                        <h5>Shipping Address</h5>
                        <div class="vh-js-shipto-address vh-shipto-address">
                            {{> address shipping.address}}
                        </div>
                    </div>
                {{/if}}
            </div>
            {{/if}}
            <div class="col-lg-6 mb-6">
                {{#if should_ship}}
                    <div class="form-group review-details">
                        <h5>Shipping Method</h5>
                        <div class="method-box-1">
                            {{shipping.method}}
                        </div>
                    </div>
                {{/if}}
                <div class="form-group review-details">
                    <h5>Payment</h5>
                    {{#if selected_card}}
                        <div class="method-box-1">
                            {{title_case selected_card.card_type}} <code>xxxx{{selected_card.last_four}}</code> <br>
                            expires on <code>{{selected_card.expiration_month}}/{{selected_card.expiration_year}}</code>
                        </div>
                    {{else}}
                        {{#ifCond platform 'stripe'}}
                            <div class="method-box-1">
                                {{title_case payment.card.brand}} <code>xxxx{{payment.card.last4}}</code> <br>
                                expires on <code>{{payment.card.exp_month}}/{{payment.card.exp_year}}</code>
                            </div>
                            <div class="method-box-2 mt-4">
                                <input type="checkbox" class="vh-js-save-payment" value="1"> Remember payment method
                            </div>
                        {{/ifCond}}
                        {{#ifCond platform 'nexiopay'}}
                            <div class="method-box-1">
                                {{title_case payment.cardType}} <code>{{payment.token.firstSix}}xxxx{{payment.token.lastFour}}</code> <br>
                                expires on <code>{{payment.card.expirationMonth}}/{{payment.card.expirationYear}}</code>
                            </div>
                            <div class="method-box-2 mt-4">
                                <input type="checkbox" class="vh-js-save-payment" value="1"> Remember payment method
                            </div>
                        {{/ifCond}}
                        {{#ifCond platform 'paypal'}}
                            <div class="method-box-1">Paying with PayPal.</code></div>
                        {{/ifCond}}
                    {{/if}}
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="col-lg-12">
                <div class="form-group">
                    <button class="btn btn-primary btn-block btn-lg vh-submit-form" type="submit">Complete Order</button>
                </div>
                <div class="terms-text-box">By clicking “Complete Order”, you agree to our <a href="">Terms of Service</a> + <a href="">Privacy policy</a>.</div>
            </div>
        </div>
    </script>

    <!---------------------------
        Sidebox (lineitems list)
    -----------------------------
        Available variables
    -----------------------------
        {
            total_items,
            cart_data
            {
                order_id,
                total,
                items
                [{
                    id,
                    product_image,
                    product_title,
                    product_description,
                    product_price,
                    quantity
                }],[...],

                totals
                {
                    product_discount
                    product_total
                    shipping
                    tax
                    total_no_discount
                    total_with_discount
                }
            }
        }
    -->
    <script class="vh-js-template-checkout-sidebox" data-priority="100" type="text/x-handlebars-template">
        <div class="vh-side-box">
            <div class="inner-box">
                <h3>Order Summary</h3>
                {{#if should_ship}}
                    {{#if shipping_amount}}
                        <div class="order-text vh-js-shipping-amount">Shipping <span>${{currency shipping_amount}}</span></div>
                    {{else}}
                        <div class="order-text vh-js-shipping-amount">Shipping <span>${{currency cart_data.totals.shipping}}</span></div>
                    {{/if}}
                {{/if}}
                <div class="order-text">Items <span>{{total_items}}</span></div>
                {{#if total_discount}}
                    <div class="order-discount vh-js-order-total-discount-amount">Discount <span>${{currency total_discount}}</span></div>
                {{/if}}
                {{#if order_total}}
                    <div class="order-total vh-js-order-total-amount">Total <span>${{currency order_total}}</span></div>
                {{else}}
                    <div class="order-total vh-js-order-total-amount">Total <span>${{currency cart_data.totals.product_total}}</span></div>
                {{/if}}
            </div>
            <div class="vh-product-scroll-box">
                {{#each cart_data.items}}
                <div class="vh-order-product vh-js-order-product-{{id}} clearfix">
                    <div class="close-box"><a class="vh-js-remove-cart-item" href="javascript:void(0);" data-id="{{id}}">&times;</a></div>
                    <div class="img-box"><a href="javascript:void(0);"><img src="{{{product_image}}}"></a></div>
                    <div class="about-product">
                        <h6 class="vh-js-cart-product-title">{{product_title}}</h6>
                        <p class="vh-js-cart-product-description">{{product_cart_label}}</p>
                        <div class="qty-box">
                            <a href="javascript:void(0);" class="vh-js-cart-decrement" data-id="{{id}}">-</a>
                            <input class="vh-cart-qty-indicator vh-js-cart-qty" type="text" placeholder="QTY {{quantity}}" disabled>
                            <a href="javascript:void(0);" class="vh-js-cart-increment" data-id="{{id}}">+</a>
                            <span class="vh-js-cart-product-amount">${{currency product_price}}</span>
                        </div>
                    </div>
                </div>
                {{/each}}
            </div>
        </div>
    </script>

    <!---------------------------
        Shipping Step
    -----------------------------
        Available variables
    -----------------------------
        {
            addresses
            {
                id,
                first_name,
                last_name,
                address_1,
                address_2,
                city,
                state,
                country,
                zip,
                phone
            }
        }
    -->
    <script class="vh-js-template-checkout-shippingform" data-priority="100" type="text/x-handlebars-template">
        <div class="form-row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>First Name *</label>
                    <input name="first_name" type="text" class="vh-form-control form-control" value="{{first_name}}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Last Name *</label>
                    <input name="last_name" type="text" class="vh-form-control form-control" value="{{last_name}}">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>Address *</label>
            <input name="address_1" type="text" class="vh-form-control form-control" value="{{address_1}}">
        </div>
        <div class="form-group">
            <label>Address Line 2 </label>
            <input name="address_2" type="text" class="vh-form-control form-control" value="{{address_2}}">
        </div>
        <div class="form-row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Country *</label>
                    <select name="country" class="vh-form-control form-control">
                        {{#selectOption country}}
                        <option value="US">United States</option>
                        {{/selectOption}}
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>State *</label>
                    <select name="state" class="vh-form-control form-control">
                        {{#selectOption state}}
                        <option value="AL">Alabama</option>
                        <option value="AK">Alaska</option>
                        <option value="AZ">Arizona</option>
                        <option value="AR">Arkansas</option>
                        <option value="CA">California</option>
                        <option value="CO">Colorado</option>
                        <option value="CT">Connecticut</option>
                        <option value="DE">Delaware</option>
                        <option value="DC">District Of Columbia</option>
                        <option value="FL">Florida</option>
                        <option value="GA">Georgia</option>
                        <option value="HI">Hawaii</option>
                        <option value="ID">Idaho</option>
                        <option value="IL">Illinois</option>
                        <option value="IN">Indiana</option>
                        <option value="IA">Iowa</option>
                        <option value="KS">Kansas</option>
                        <option value="KY">Kentucky</option>
                        <option value="LA">Louisiana</option>
                        <option value="ME">Maine</option>
                        <option value="MD">Maryland</option>
                        <option value="MA">Massachusetts</option>
                        <option value="MI">Michigan</option>
                        <option value="MN">Minnesota</option>
                        <option value="MS">Mississippi</option>
                        <option value="MO">Missouri</option>
                        <option value="MT">Montana</option>
                        <option value="NE">Nebraska</option>
                        <option value="NV">Nevada</option>
                        <option value="NH">New Hampshire</option>
                        <option value="NJ">New Jersey</option>
                        <option value="NM">New Mexico</option>
                        <option value="NY">New York</option>
                        <option value="NC">North Carolina</option>
                        <option value="ND">North Dakota</option>
                        <option value="OH">Ohio</option>
                        <option value="OK">Oklahoma</option>
                        <option value="OR">Oregon</option>
                        <option value="PA">Pennsylvania</option>
                        <option value="RI">Rhode Island</option>
                        <option value="SC">South Carolina</option>
                        <option value="SD">South Dakota</option>
                        <option value="TN">Tennessee</option>
                        <option value="TX">Texas</option>
                        <option value="UT">Utah</option>
                        <option value="VT">Vermont</option>
                        <option value="VA">Virginia</option>
                        <option value="WA">Washington</option>
                        <option value="WV">West Virginia</option>
                        <option value="WI">Wisconsin</option>
                        <option value="WY">Wyoming</option>
                        {{/selectOption}}
                    </select>
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>City *</label>
                    <input name="city" type="text" class="vh-form-control form-control" value="{{city}}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>ZIP *</label>
                    <input name="zip" type="text" class="vh-form-control form-control" value="{{zip}}">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>Phone Number</label>
            <input name="phone" type="text" class="vh-form-control form-control" value="{{phone}}">
        </div>
        <div class="form-group">
            <div class="form-check form-check-inline ml-4">
                <input name="primary_address" class="form-check-input vh-js-make-default" type="checkbox" value="1">
                <label class="form-check-label">Make Default</label>
            </div>
        </div>
        <div class="form-group">
            <button class="btn btn-primary btn-block btn-lg vh-submit-form" type="submit">Continue</button>
        </div>
    </script>

    <!---------------------------
        Payment Step
    -----------------------------
        Available variables
    -----------------------------
        {
            shipping_address
            {
                id,
                first_name,
                last_name,
                address_1,
                address_2,
                city,
                state,
                country,
                zip,
                phone
            },
            shipping_options
            {
                rate,
                method_code,
                method_label,
                rate_label
            }
        }
    -->
    <script class="vh-js-template-checkout-shippingdetails-form" data-priority="100" type="text/x-handlebars-template">
        <div class="form-row">
            <div class="col-lg-6 mb-6">
                <div class="review-details">
                    <h5>Shipping Address</h5>
                    <div class="vh-js-shipto-address vh-shipto-address">
                        {{> address shipping_address}}
                    </div>
                    <div>
                        <a href="javascript:void(0);" class="vh-js-change-shipping">Change Shipping Address</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 mb-6">
                <div class="form-group review-details">
                    <h5>Shipping Method</h5>
                    {{#each shipping_options}}
                    <div class="method-box">
                        <div class="seclect-check-box">
                            <input name="shipping_option" type="radio" class="vh-js-shipping-option-input" data-label="{{method_label}}" data-rate="{{rate}}" data-method="{{method_code}}">
                        </div>
                        {{method_label}}
                        <div class="price-free">{{rate_label}}</div>
                    </div>
                    {{/each}}
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="col-lg-6 mb-6">
                <div class="review-details">
                    <h5>Promo Code</h5>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Enter promo code">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="button">Apply</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="col-lg-12">
                <div class="form-group">
                    <button class="btn btn-primary btn-block btn-lg vh-submit-form" type="submit">Enter Payment and Submit Order</button>
                </div>
                <!--<div class="terms-text-box">By clicking “Submit Order”, you agree to our <a href="javascript:void(0);">Terms of Service</a> + <a href="javascript:void(0);">Privacy policy</a>.</div>-->
                <div>
                    <a href="javascript:void(0);" class="vh-js-update-shipping"><< Change Shipping Address</a>
                </div>
            </div>
        </div>
    </script>

    <!------------------------------------
        Select From Shipping Addresses
    --------------------------------------
        Available variables
    --------------------------------------
        {
            shipping_addresses
            [{
                id,
                first_name,
                last_name,
                address_1,
                address_2,
                city,
                state,
                country,
                zip,
                phone
            }. {...}],
        }
    -->
    <script class="vh-js-template-checkout-shipping-addresses" data-priority="100" type="text/x-handlebars-template">
        <div class="modal-body">
            <div class="row">
                <h3 class="title">Select Shipping Address</h3>
            </div>
            <div class="row">
                {{#each addresses}}
                <div class="col-4">
                    <div class="vh-shipping-address-box vh-js-shipping-address-box {{#ifCond id ../selected_shipping_id}}selected{{/ifCond}}" data-id="{{id}}">

                        {{#ifCond id ../selected_shipping_id}}
                        {{else}}
                        {{#unless is_primary}}
                        <a href="javascript:void(0);" class="vh-address-remove vh-js-address-remove" data-id="{{id}}">&times;</a>
                        {{/unless}}
                        {{/ifCond}}

                        <a class="vh-shipping-address-link vh-js-shipping-address-link" href="javascript:void(0);" data-address="{{json_string this}}">
                            <div class="vh-shipping-address-content">
                                {{> address}}
                            </div>
                            <div class="shipping-address-actions clearfix">
                                <span class="pull-left">
                                    <a href="javascript:void(0);" class="vh-js-edit-shipping-address">Edit</a>
                                </span>
                                {{#if is_primary}}
                                <span class="pull-right badge badge-secondary">Default</span>
                                {{/if}}
                            </div>
                        </a>
                    </div>
                </div>
                {{/each}}
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary btn-block btn-lg vh-js-add-shipping-address">Add New</button>
        </div>
    </script>

    <!------------------------------------
        Edit Shipping Address
    --------------------------------------
        Available variables
    --------------------------------------
        {
            id,
            first_name,
            last_name,
            address_1,
            address_2,
            city,
            state,
            country,
            zip,
            phone
        }
    -->
    <script class="vh-js-template-checkout-edit-shipping-address" data-priority="100" type="text/x-handlebars-template">
        <form class="vh-js-checkout-edit-shipping-address-form" method="post">
            <input type="hidden" name="address_id" value="{{id}}">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="title">Edit Shipping Address</h3>
                    </div>
                </div>
                <div class="vh-form-fields">
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>First Name *</label>
                                <input name="first_name" type="text" class="vh-form-control form-control" value="{{first_name}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Last Name *</label>
                                <input name="last_name" type="text" class="vh-form-control form-control" value="{{last_name}}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Address *</label>
                        <input name="address_1" type="text" class="vh-form-control form-control" value="{{address_1}}">
                    </div>
                    <div class="form-group">
                        <label>Address Line 2 </label>
                        <input name="address_2" type="text" class="vh-form-control form-control" value="{{address_2}}">
                    </div>
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Country *</label>
                                <select name="country" class="vh-form-control form-control">
                                    {{#selectOption country}}
                                    <option value="US">United States</option>
                                    {{/selectOption}}
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>State *</label>
                                <select name="state" class="vh-form-control form-control">
                                    {{#selectOption state}}
                                    <option value="AL">Alabama</option>
                                    <option value="AK">Alaska</option>
                                    <option value="AZ">Arizona</option>
                                    <option value="AR">Arkansas</option>
                                    <option value="CA">California</option>
                                    <option value="CO">Colorado</option>
                                    <option value="CT">Connecticut</option>
                                    <option value="DE">Delaware</option>
                                    <option value="DC">District Of Columbia</option>
                                    <option value="FL">Florida</option>
                                    <option value="GA">Georgia</option>
                                    <option value="HI">Hawaii</option>
                                    <option value="ID">Idaho</option>
                                    <option value="IL">Illinois</option>
                                    <option value="IN">Indiana</option>
                                    <option value="IA">Iowa</option>
                                    <option value="KS">Kansas</option>
                                    <option value="KY">Kentucky</option>
                                    <option value="LA">Louisiana</option>
                                    <option value="ME">Maine</option>
                                    <option value="MD">Maryland</option>
                                    <option value="MA">Massachusetts</option>
                                    <option value="MI">Michigan</option>
                                    <option value="MN">Minnesota</option>
                                    <option value="MS">Mississippi</option>
                                    <option value="MO">Missouri</option>
                                    <option value="MT">Montana</option>
                                    <option value="NE">Nebraska</option>
                                    <option value="NV">Nevada</option>
                                    <option value="NH">New Hampshire</option>
                                    <option value="NJ">New Jersey</option>
                                    <option value="NM">New Mexico</option>
                                    <option value="NY">New York</option>
                                    <option value="NC">North Carolina</option>
                                    <option value="ND">North Dakota</option>
                                    <option value="OH">Ohio</option>
                                    <option value="OK">Oklahoma</option>
                                    <option value="OR">Oregon</option>
                                    <option value="PA">Pennsylvania</option>
                                    <option value="RI">Rhode Island</option>
                                    <option value="SC">South Carolina</option>
                                    <option value="SD">South Dakota</option>
                                    <option value="TN">Tennessee</option>
                                    <option value="TX">Texas</option>
                                    <option value="UT">Utah</option>
                                    <option value="VT">Vermont</option>
                                    <option value="VA">Virginia</option>
                                    <option value="WA">Washington</option>
                                    <option value="WV">West Virginia</option>
                                    <option value="WI">Wisconsin</option>
                                    <option value="WY">Wyoming</option>
                                    {{/selectOption}}
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>City *</label>
                                <input name="city" type="text" class="vh-form-control form-control" value="{{city}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>ZIP *</label>
                                <input name="zip" type="text" class="vh-form-control form-control" value="{{zip}}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Phone Number</label>
                        <input name="phone" type="text" class="vh-form-control form-control" value="{{phone}}">
                    </div>
                    <div class="form-group">
                        <div class="form-check form-check-inline ml-4">
                            <input name="primary_address" class="form-check-input vh-js-make-default" type="checkbox" value="1" {{#if is_primary}}checked{{/if}}>
                            <label class="form-check-label">Make Default</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="btn-group full w-100" role="group">
                    <button class="btn btn-secondary btn-block btn-lg vh-cancel vh-js-cancel">Cancel</button>
                    <button class="btn btn-primary btn-block btn-lg" type="submit">Update</button>
                </div>
            </div>
        </form>
    </script>


    <!------------------------------------
        Add Shipping Address
    --------------------------------------
    -->
    <script class="vh-js-template-checkout-add-shipping-address" data-priority="100" type="text/x-handlebars-template">
        <form class="vh-js-checkout-add-shipping-address-form" method="post">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="title">Add Shipping Address</h3>
                    </div>
                </div>
                <div class="vh-form-fields">
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>First Name *</label>
                                <input name="first_name" type="text" class="vh-form-control form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Last Name *</label>
                                <input name="last_name" type="text" class="vh-form-control form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Address *</label>
                        <input name="address_1" type="text" class="vh-form-control form-control">
                    </div>
                    <div class="form-group">
                        <label>Address Line 2 </label>
                        <input name="address_2" type="text" class="vh-form-control form-control">
                    </div>
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Country *</label>
                                <select name="country" class="vh-form-control form-control">
                                    <option value="US">United States</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>State *</label>
                                <select name="state" class="vh-form-control form-control">
                                    <option value="AL">Alabama</option>
                                    <option value="AK">Alaska</option>
                                    <option value="AZ">Arizona</option>
                                    <option value="AR">Arkansas</option>
                                    <option value="CA">California</option>
                                    <option value="CO">Colorado</option>
                                    <option value="CT">Connecticut</option>
                                    <option value="DE">Delaware</option>
                                    <option value="DC">District Of Columbia</option>
                                    <option value="FL">Florida</option>
                                    <option value="GA">Georgia</option>
                                    <option value="HI">Hawaii</option>
                                    <option value="ID">Idaho</option>
                                    <option value="IL">Illinois</option>
                                    <option value="IN">Indiana</option>
                                    <option value="IA">Iowa</option>
                                    <option value="KS">Kansas</option>
                                    <option value="KY">Kentucky</option>
                                    <option value="LA">Louisiana</option>
                                    <option value="ME">Maine</option>
                                    <option value="MD">Maryland</option>
                                    <option value="MA">Massachusetts</option>
                                    <option value="MI">Michigan</option>
                                    <option value="MN">Minnesota</option>
                                    <option value="MS">Mississippi</option>
                                    <option value="MO">Missouri</option>
                                    <option value="MT">Montana</option>
                                    <option value="NE">Nebraska</option>
                                    <option value="NV">Nevada</option>
                                    <option value="NH">New Hampshire</option>
                                    <option value="NJ">New Jersey</option>
                                    <option value="NM">New Mexico</option>
                                    <option value="NY">New York</option>
                                    <option value="NC">North Carolina</option>
                                    <option value="ND">North Dakota</option>
                                    <option value="OH">Ohio</option>
                                    <option value="OK">Oklahoma</option>
                                    <option value="OR">Oregon</option>
                                    <option value="PA">Pennsylvania</option>
                                    <option value="RI">Rhode Island</option>
                                    <option value="SC">South Carolina</option>
                                    <option value="SD">South Dakota</option>
                                    <option value="TN">Tennessee</option>
                                    <option value="TX">Texas</option>
                                    <option value="UT">Utah</option>
                                    <option value="VT">Vermont</option>
                                    <option value="VA">Virginia</option>
                                    <option value="WA">Washington</option>
                                    <option value="WV">West Virginia</option>
                                    <option value="WI">Wisconsin</option>
                                    <option value="WY">Wyoming</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>City *</label>
                                <input name="city" type="text" class="vh-form-control form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>ZIP *</label>
                                <input name="zip" type="text" class="vh-form-control form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Phone Number</label>
                        <input name="phone" type="text" class="vh-form-control form-control">
                    </div>
                    <div class="form-group">
                        <div class="form-check form-check-inline">
                            <input name="primary_address" class="form-check-input vh-js-make-default" type="checkbox" value="1">
                            <label class="form-check-label">Make Default</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer vh-btn-group">
                <button class="btn btn-primary btn-block btn-lg vh-cancel vh-js-cancel">Cancel</button>
                <button class="btn btn-primary btn-block btn-lg" type="submit">Save</button>
            </div>
        </form>
    </script>

    <!-- Stripe Payment Form -->
    <script class="vh-js-template-checkout-stripepaymentform" data-priority="100" type="text/x-handlebars-template">
        <div class="form-row">
            <div class="col-md-12">
                <div id="vh-js-stripe-card-errors" role="alert"></div>
            </div>
        </div>
        <div class="form-row">
            <div class="col-md-12">
                <div class="form-group">
                    <input type="hidden" id="stripe_token" name="stripe_token">
                    <input type="hidden" id="stripe_is_valid" name="stripe_is_valid">
                    <div class="vh-js-stripe-card-holder mb-5"></div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <button class="btn btn-primary btn-block btn-lg vh-submit-form" type="submit">Continue</button>
        </div>
    </script>

    <!------------------------------------
        Shipping Address
    --------------------------------------
    -->
    <script class="vh-js-partial-checkout-address" data-priority="100" type="text/x-handlebars-template">
        <div class="vh-address-block">
            <span class="line bold"><strong>{{first_name}} {{last_name}}</strong></span>
            <span class="line">{{address_1}}</span>
            {{#if address_2}}
            <span class="line">{{address_2}}</span>
            {{/if}}
            <span class="line">{{city}}, {{state}}</span>
            <span class="line">{{zip}}</span>
            <span class="line">{{phone}}</span>
        </div>
    </script>

</div>
