function VaharaCheckout($) {

    this.init = function () {
        //console.log('VaharaCheckout Initialized');

        var _settings = [];
        var _orderId = null;
        var _addedProducts = [];
        var _templates = {
            sideBox: '.vh-js-template-checkout-sidebox',
            shippingForm: '.vh-js-template-checkout-shippingform',
            shippingDetailsForm: '.vh-js-template-checkout-shippingdetails-form',
            selectCard: '.vh-js-template-checkout-select-card',
            reviewForm: '.vh-js-template-checkout-reviewform',
            stripePaymentForm: '.vh-js-template-checkout-stripepaymentform',
	    cloverPaymentForm: '.vh-js-template-checkout-cloverpaymentform',
            orderDetails: '.vh-js-template-checkout-orderdetails',
            shippingAddressList: '.vh-js-template-checkout-shipping-addresses',
            editShippingAddress: '.vh-js-template-checkout-edit-shipping-address',
            addShippingAddress: '.vh-js-template-checkout-add-shipping-address',

            partials: {
                address: '.vh-js-partial-checkout-address'
            }
        };
        var _formInstances = {
            shipping: null,
            shippingDetails: null,
            payment: null,
            shippingModal: {
                addAddress: null,
                editAddress: null
            }
        };
        var _checkout = {
            currentStep: null,
            order: { non_shipping: false },
            shipping: {
                options: [],
                available_addresses: [],
                selected_address: null,
                label: null,
                method: null,
                amount: null,
                address: {
                    primary_address: 0,
                    address_id: null,
                    first_name: null,
                    last_name: null,
                    address_1: null,
                    address_2: null,
                    city: null,
                    country: null,
                    phone: null,
                    state: null,
                    zip: null
                },
                valid: false
            },
            payment: {
                shipping_options: [],
                valid: false,
                response: null,
                data: null,
                savedCards: [],
                selectedCardId: 0
            },
            review: {
                valid: false
            },
            order_complete: {
                valid: false
            },
            stripe: {},
	    clover: {}
        };

        window.checkoutRun = function(run) {
            eval(run);
        };

        var nexioPayCss = window.vahara.clientJqueryBootstrap4Path + '/checkout/nexiopay.css?_=' + (new Date()).getTime();
        if (window.vaharaNexioPayCss) {
            nexioPayCss = window.vaharaNexioPayCss;
        }
        var steps = ['shipping_address', 'shipping_details', 'select_card', 'payment', 'review', 'order_complete'];
        var switchableSteps = ['shipping_address', 'shipping_details', 'payment', 'review'];

        // Should we allow the sidebar cart to show up on checkout page?
        // We should have a more standard method to do this
        // $('.vh-element-cart').empty();
        // TODO we should trigger a vh-checkout:should-empty-cart

        // Register the address partial
        Handlebars.registerPartial('address', $(_templates.partials.address).html());

        /**
         * On cart empty event
         */
        v.onEvent('vh-checkout:cart-empty', function () {
            console.log('vh-checkout:cart-empty!');

            if(_checkout.currentStep === 'order_complete') return false;

            if (typeof _settings === 'object') {
                showLoading(true);
                setTimeout(function () {
                    if (_settings.store_shopping_page) {
                        window.location = _settings.store_shopping_page;
                    } else {
                        window.location = '/';
                    }
                }, 300);
            } else {
                console.error('There was an error redirecting to shopping page.');
                window.location = '/';
            }
        });

        /**
         * Handle the item-updated event
         */
        v.onEvent('vh-checkout:item-updated', function (data) {
            console.log('vh-checkout:item-updated');

            renderSideBox(_getSelectedShippingRate());
        });

        /**
         * Handle the item-removed event
         */
        v.onEvent('vh-checkout:item-removed', function (data) {
            console.log('vh-checkout:item-removed');

            renderSideBox(_getSelectedShippingRate());
        });

        /**
         * On payment success
         */
        v.onEvent('vh-checkout:payment-success', function (data) {
            console.log('vh-checkout:payment-success');

            _checkout.order_complete.valid = true;
            $('.vh-js-cart-qty').text(0);

            initStep('order_complete', data.data.data.order);
        });

        /**
         * On payment failed
         */
        v.onEvent('vh-checkout:payment-failed', function () {
            console.log('vh-checkout:payment-failed');

            window.vahara.Alert.alert('Error', 'Payment has failed!');
        });

        /**
         * Change shipping address
         */
        $(document).on('click', '.vh-client-component-checkout .vh-js-shipping-address-link', function (e) {
            e.preventDefault();

            var address = $(e.currentTarget).data('address');

            if (address.id !== _checkout.shipping.address.address_id) {
                _updateShippingAddressDisplay(address);

                $('.vh-js-shipping-mp-modal .vh-js-shipping-address-box').removeClass('selected');
                $(e.currentTarget).parent().addClass('selected');

                saveAddressToOrder().then(function () {
                    _hideShippingModal();
                });
            }
        });

        /**
         * Cancel adding/editing address
         */
        $(document).on('click', '.vh-client-component-checkout .vh-js-shipping-mp-modal .vh-js-cancel', function (e) {
            e.preventDefault();

            _resetModalForms();
            _selectShipping();
        });

        /**
         * Add new shipping address
         */
        $(document).on('click', '.vh-client-component-checkout .vh-js-add-shipping-address', function (e) {
            e.preventDefault();

            _addShipping();
        });

        /**
         * Edit shipping address
         */
        $(document).on('click', '.vh-client-component-checkout .vh-js-edit-shipping-address', function (e) {
            e.preventDefault();

            var address = $(e.currentTarget).parent().find('.vh-js-shipping-address-link').data('address');

            _editShipping(address);
        });

        /**
         * Change shipping address
         */
        $(document).on('click', '.vh-client-component-checkout .vh-js-change-shipping', function (e) {
            e.preventDefault();

            _selectShipping();
        });

        /**
         * Remove an addreess
         */
        $(document).on('click', '.vh-client-component-checkout .vh-js-address-remove', function (e) {
            e.preventDefault();

            if (confirm("Are you sure you want to delete this address?")) {
                var addressId = $(this).data('id');
                v.callApi('/address/delete', {
                    data: {address_id: addressId},
                    success: function (response) {
                        if (response.success === true) {
                            var addressBox = $(".vh-client-component-checkout .vh-js-shipping-address-box[data-id='${addressId}']");
                            addressBox.fadeOut(200, function () {
                                addressBox.parent().remove();
                            });
                        } else {
                            console.error('There was an error removing the address.');
                        }
                    }
                });
            }
        });

        /**
         * Decrement item quantity
         */
        $(document).on('click', '.vh-client-component-checkout .vh-js-cart-decrement', function (e) {
            e.preventDefault();
            e.stopPropagation();

            var itemId = $(this).data('id');
            updateItemQuantity(itemId, 'D');
        });

        /**
         * Increment item quantity
         */
        $(document).on('click', '.vh-client-component-checkout .vh-js-cart-increment', function (e) {
            e.preventDefault();
            e.stopPropagation();

            var itemId = $(this).data('id');
            updateItemQuantity(itemId, 'I');
        });

        /**
         * When the product X (remove item) element is clicked
         */
        $(document).on('click', '.vh-client-component-checkout .vh-js-remove-cart-item', function (e) {
            e.preventDefault();
            e.stopPropagation();

            var itemId = $(this).data('id');
            removeCartItem(itemId);
        });

        /**
         * When the user clicks "Change Shipping Address" on the payment step
         */
        $(document).on('click', '.vh-client-component-checkout .vh-js-update-shipping', function (e) {
            e.preventDefault();

            switchStep('shipping_details');
        });

        /**
         * Update the total and shipping rate based on user selection
         */
        $(document).on('click', '.vh-client-component-checkout .vh-js-shipping-option-input', function (e) {
            var el = $(this);

            _checkout.shipping.method = el.data('method');
            _checkout.shipping.amount = el.data('rate');
            _checkout.shipping.label = el.data('label');

            renderSideBox($(this).data('rate'));
        });

        $(document).on('click', '.vh-client-component-checkout .vh-js-step-switch', function (e) {
            e.preventDefault();

            var el = $(e.currentTarget);
            var step = el.data('step');

            if(step.indexOf(',') !== -1) {
                step = step.split(',')[1];
            }

            if(_checkout.currentStep === 'order_complete') return false;

            if(step !== _checkout.currentStep && $.inArray(step, switchableSteps) !== -1) {
                switch (step) {
                    case 'payment':
                        if((!_checkout.shipping.valid || _checkout.shipping.method !== null) && !_checkout.order.non_shipping) {
                            window.vahara.Alert.alert('Incomplete Shipping Details', 'Please complete the shipping details before proceeding!');
                        } else {
                            updateShippingMethodAndLoadToken();
                        }
                        break;

                    case 'review':
                        if(!_checkout.review.valid) {
                            window.vahara.Alert.alert('Incomplete Details', 'Please complete the previous steps before continuing');
                        } else {
                            initStep(step);
                        }
                        break;

                    default:
                        switchStep(step);
                }
            }
        });

        /**
         * Hide the overlay and payment form on click
         */
        $(document).on('click', '.vh-client-component-checkout.loading .vh-overlay', function () {
            var modal = $('.vh-js-payment-modal');
            if (modal.hasClass('visible')) {
                showLoading(false);
                showPaymentModal(false);
            }
        });

        /**
         * Save the card id that the user selects
         */
        $(document).on('click', '.vh-js-payment-select-container .chkPaymentCard', function(e) {
            _checkout.payment.selectedCardId = $(e.currentTarget).val();
        });

        /**
         * Hide the overlay and payment form on escape key
         */
        $(window).on('keyup', function (e) {
            if (e.keyCode === 27) {
                var modal = $('.vh-js-payment-modal');
                if (modal.hasClass('visible')) {
                    modal.removeClass('visible');
                    showLoading(false);
                }
            }
        });

        /**
         * Reset all modal forms when the modal is closed
         */
        $(document).on('hide.bs.modal', '.vh-js-shipping-mp-modal', function () {
            _resetModalForms();
        });

        /**
         * PRIVATE METHODS
         */
        var getTokenForNewPayment = function() {
            v.callApi('/gateway/token', {
                data: {
                    order_id: _orderId,
                    nexiopay_options: {
                        css: nexioPayCss,
                        show_billing: 1
                    }
                },
                before: function() {
                    showLoading(true);
                },
                success: function (response) {
                    if (response.success === true) {
                        _checkout.payment.processor = _settings.payments_platform;
                        _checkout.payment.response = response;
                        _checkout.payment.valid = true;

                        initStep('payment', {platform: _settings.payments_platform});
                    } else {
                        if (response.error.message) {
                            alert(response.error.message);
                        }
                        console.log('error with token');
                        return;
                        if (_settings.store_shopping_page) {
                            window.location = _settings.store_shopping_page;
                        } else {
                            window.location = '/';
                        }
                    }
                }
            });
        };

        /**
         * Initialize Step
         *
         * @param stepName
         * @param arguments
         */
        function initStep(stepName, arguments) {
            switch (stepName) {
                case 'shipping_address':
                    switchStep(stepName);
                    initShippingStep(arguments);
                    break;
                case 'shipping_details':
                    switchStep(stepName);
                    initShippingDetailsStep(arguments);
                    break;
                case 'select_card':
                    switchStep(stepName);
                    initSelectCardStep(arguments);
                    break;
                case 'payment':
                    switchStep(stepName);
                    initPaymentStep(arguments);
                    break;
                case 'review':
                    switchStep(stepName);
                    initReviewStep(arguments);
                    break;
                case 'order_complete':
                    switchStep(stepName);
                    initOrderCompleteStep(arguments);
                    break;
                default: {
                    console.log("Cannot initialize step: " + stepName);
                }
            }

            updateStepProgressUI();

            renderSideBox();
        }

        /**
         * Initialize formvalidation
         *
         * @param vhFormClass
         * @param fieldValidators
         * @param successCallback
         * @param showIcons
         * @returns {*}
         */
        function initFormValidation(vhFormClass, fieldValidators, successCallback, showIcons) {
            if (typeof showIcons === 'undefined') showIcons = true;
            var _plugins = {
                trigger: new FormValidation.plugins.Trigger({event: 'submit'}),
                bootstrap: new FormValidation.plugins.Bootstrap(),
                submitButton: new FormValidation.plugins.SubmitButton(),
                tooltip: new FormValidation.plugins.Tooltip({
                    trigger: 'hover'
                })
            };

            if (showIcons) {
                _plugins.icon = new FormValidation.plugins.Icon({
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
                });
            }

            return FormValidation.formValidation(
                document.querySelector(vhFormClass), {
                    fields: fieldValidators,
                    plugins: _plugins
                }
            ).on('core.form.valid', function () {
                successCallback();
            });
        }

        /**
         * Shipping Details Step
         */
        function initShippingDetailsStep() {
            showLoading(false);

            var _data = {shipping_address: _checkout.shipping.address};

            var request = v.callApi('/shipping/options', {
                data: {order_id: _orderId, visitor_id: v.getVisitorId()},
                success: function (response) {
                    if (response.success === true) {
                        _checkout.shipping.options = response.data.shipping_options;
                        _data.shipping_options = response.data.shipping_options;
                    }
                }
            });

            request.then(function () {
                var shippingDetailsHtml = v.compileTemplate(_templates.shippingDetailsForm, _data);
                $('.vh-client-component-checkout')
                    .find('.vh-js-shipping-details-form')
                    .empty()
                    .append(shippingDetailsHtml);

                initShippingDetailsValidation();

                // Select shipping method as if it were pre-selected
                if (_checkout.shipping.method !== null) {
                    setTimeout(function () {
                        $('.vh-client-component-checkout')
                            .find('.vh-js-shipping-option-input[data-method="' + _checkout.shipping.method + '"]')
                            .trigger('click');
                    }, 500);
                }
            });
        }

        /**
         * Initialize Select Card Step
         *
         * @param payment
         */
        function initSelectCardStep(payment) {
            var html = v.compileTemplate(_templates.selectCard, {cards: _checkout.payment.savedCards});

            $('.vh-js-payment-select-container').empty().append(html);

            initSelectCardValidation();

            $('.vh-js-payment-select-container .chkPaymentCard:checked').trigger('click');
        }

        /**
         * Initialize Payment Step
         */
        function initPaymentStep(payment) {

            _switchPaymentPlatformSection(payment.platform);

            switch (payment.platform) {
                case 'stripe':
                    _stripeModal(_checkout.payment.response);
                    break;

                case 'clover':
                    _cloverModal(_checkout.payment.response);
                    break;

                case 'nexiopay':
                    _nexioPayModal(_checkout.payment.response);
                    break;

                case 'paypal':
                    _paypalModal(_checkout.payment.response);
                    break;
            }
        }

        /**
         * Initialize Payment Step
         */
        function initReviewStep() {
            var shippingRateLabel = _checkout.shipping.amount === 0 ? 'FREE' : '$' + _checkout.shipping.amount;
            var selectedCard = null;

            if (_checkout.payment.selectedCardId !== 'NEW') {
                selectedCard = _.find(_checkout.payment.savedCards, function(o) { return o.id == _checkout.payment.selectedCardId; });
            }

            var _data = {
                platform: _settings.payments_platform,
                shipping: {
                    address: _checkout.shipping.address,
                    method: _checkout.shipping.label + ' - ' + shippingRateLabel
                },
                payment: _checkout.payment.data,
                selected_card: selectedCard,
                should_ship: !_checkout.order.non_shipping
            };

            initReviewFormValidation();

            var reviewHtml = v.compileTemplate(_templates.reviewForm, _data);

            $('.vh-client-component-checkout')
                .find('.vh-js-review-form')
                .empty()
                .append(reviewHtml);

           showLoading(false);
        }

        /**
         * Initialize the order completed step
         *
         * @param orderDetails
         */
        function initOrderCompleteStep(orderDetails)
        {
            if(typeof _settings.store_shopping_page !== 'undefined' && _settings.store_shopping_page !== null) {
                orderDetails.shop_page = _settings.store_shopping_page;
            }

            var orderHtml = v.compileTemplate(_templates.orderDetails, orderDetails);

            $('.vh-client-component-checkout')
                .find('.vh-js-checkout-order-complete')
                .empty()
                .append(orderHtml);
        }

        /**
         * Initialize Shipping Step
         */
        function initShippingStep() {

            var shippingHtml = v.compileTemplate(_templates.shippingForm, _checkout.shipping.address);

            var request = v.callApi('/orders/get-shipping-details', {
                data: {order_id: _orderId, visitor_id: v.getVisitorId()},
                success: function (response) {
                    if (response.success === true) {
                        _checkout.shipping.valid = true;
                        _checkout.shipping.method = response.data.details.shipping_method;
                        _checkout.shipping.amount = response.data.details.shipping_amount;
                        _checkout.shipping.address = response.data.details.address;

                        // Because we have some naming interchangeability issues with address_1 and address1
                        _checkout.shipping.address.address1 = _checkout.shipping.address.address_1;
                        _checkout.shipping.address.address2 = _checkout.shipping.address.address_2;

                        shippingHtml = v.compileTemplate(_templates.shippingForm, _checkout.shipping.address);
                    }
                }
            });

            request.then(function () {
                $('.vh-client-component-checkout')
                    .find('.vh-js-shipping-form')
                    .empty()
                    .append(shippingHtml);
                initShippingValidation();
                if (_checkout.shipping.valid) {
                    initStep('shipping_details');
                }
            });
        }

	/**
         * Initialize Clover Payment Validation
         */
        function initCloverPaymentValidation(cloverContainer) {
            _formInstances.payment = initFormValidation('.vh-js-clover-payment-form', {
                clover_is_valid: {
                    validators: {
                        callback: {
                            message: 'Invalid Card Details',
                            callback: function (value, validator, $field) {
                                return _checkout.payment.valid === true;
                            }
                        }
                    }
                }
            }, function () {
                showLoading(true);
                _checkout.clover.instance.createToken()
                    .then(function(result) {
                        showLoading(false);
                        if (result.errors) {
                            for (const [errorFieldKey, errorMessage] of Object.entries(result.errors)) {
                                switch (errorFieldKey)
                                {
                                    case 'CARD_NUMBER':
                                        cloverContainer.find('#card-number-errors').html(errorMessage);
                                        break;
                                    case 'CARD_DATE':
                                        cloverContainer.find('#card-date-errors').html(errorMessage);
                                        break;
                                    case 'CARD_CVV':
                                        cloverContainer.find('#card-cvv-errors').html(errorMessage);
                                        break;
                                    case 'CARD_POSTAL_CODE':
                                        cloverContainer.find('#card-postal-code-errors').html(errorMessage);
                                        break;
                                }
                            }
                            _checkout.payment.valid = false;
                            _formInstances.payment.validateField('clover_is_valid');
                        } else {
                            _checkout.payment.valid = true;
                            _checkout.payment.data = result;
                            initStep('review');
                        }
                    }).catch(function(data){
                        showLoading(false);
                        _checkout.payment.valid = false;
                        _formInstances.payment.validateField('clover_is_valid');
                    });
            }, false);
        }

        /**
         * Initialize Stripe Payment Validation
         */
        function initStripePaymentValidation() {
            _formInstances.payment = initFormValidation('.vh-js-stripe-payment-form', {
                stripe_is_valid: {
                    validators: {
                        callback: {
                            message: 'Invalid Card Details',
                            callback: function (value, validator, $field) {
                                return _checkout.payment.valid === true;
                            }
                        }
                    }
                }
            }, function () {

                showLoading(true);

                _checkout.stripe.instance.createToken(_checkout.stripe.card).then(function(result) {
                    //showLoading(false);

                    if(result.error) {
			showLoading(false);
			console.log('error');
			console.log(result.error);

			if (result.error.code == 'incomplete_zip') {
			   alert("Please check the zip code on your payment to continue.");
			}
			
                        _checkout.payment.valid = false;
                        _formInstances.payment.validateField('stripe_is_valid');
                    } else {
                        _checkout.payment.valid = true;
                        _checkout.payment.data = result.token;

                        initStep('review');
                    }
                });
            }, false);
        }

        /**
         * Initialize Select Card Form Validation
         */
        function initSelectCardValidation() {
            _formInstances.shippingDetails = initFormValidation('.vh-js-select-card-form', {
                payment_card: {
                    validators: {
                        notEmpty: {message: 'Please select an option to continue'}
                    }
                }
            }, function () {
                if (_checkout.payment.selectedCardId === 'NEW') {
                    getTokenForNewPayment();
                } else {
                    showLoading(false);

                    _checkout.payment.valid = true;
                    _checkout.payment.data = null;
                    _checkout.review.valid = true;

                    initStep('review');
                }
            }, false);
        }

        /**
         * Initialize Shipping Method Validation
         */
        function initShippingDetailsValidation() {
            _formInstances.shippingDetails = initFormValidation('.vh-js-checkout-shipping-details-form', {
                shipping_option: {
                    validators: {
                        notEmpty: {message: 'Please select a shipping method'}
                    }
                }
            }, function () {
                updateShippingMethodAndLoadToken();
            }, false);
        }

        /**
         * Initialize Shipping Method Validation
         */
        function initReviewFormValidation() {
            var form = $('.vh-client-component-checkout .vh-js-checkout-review-form');

            form.on('submit', function (e) {
                e.preventDefault();

		    console.log('showing loading');
                showLoading(true);

                v.callApi('/gateway/process', {
                    data: {
                        visitor_id: v.getVisitorId(),
                        order_id: _orderId,
                        token: 'notok',
                        email: window.vahara.userEmail,
                        save_payment: form.find('.vh-js-save-payment').prop('checked') ? 1 : 0,
                        payment_data: _checkout.payment.data,
                        selected_card: _checkout.payment.selectedCardId
                    },
                    success: function (response) {
			    console.log('ok got a reply');
                        if (response.success === true) {
                            // Complete the order based on available payment data
                            // The completeOrder method will trigger SUCCESS or FAILED payment
                            // events
                            if(typeof response.data.payment_response !== 'undefined') {
                                completeOrder(response.data.payment_response);
                            } else {
                                completeOrder(response);
                            }
                        } else {
                            window.vahara.Alert.alert('Error', response.message);
                        }
                    }
                });
            });
        }

        /**
         * Initialize Shipping Validation
         */
        function initShippingValidation() {
            _formInstances.shipping = initFormValidation('.vh-js-checkout-shipping-address-form', {
                first_name: {
                    validators: {
                        notEmpty: {message: 'First name is required'}
                    }
                },
                last_name: {
                    validators: {
                        notEmpty: {message: 'Last name is required'}
                    }
                },
                address_1: {
                    validators: {
                        notEmpty: {message: 'Address line 1 is required'}
                    }
                },
                city: {
                    validators: {
                        notEmpty: {message: 'City is required'}
                    }
                },
                state: {
                    validators: {
                        notEmpty: {message: 'State is required'}
                    }
                },
                country: {
                    validators: {
                        notEmpty: {message: 'Country is required'}
                    }
                },
                zip: {
                    validators: {
                        notEmpty: {message: 'Zip code is required'},
                        regexp: {
                            regexp: /^[0-9]{3,5}$/,
                            message: 'Invalid zip code'
                        }
                    }
                }
            }, function () {

                showLoading(true);

                var form = $('.vh-js-checkout-shipping-address-form');
                var formDataArray = form.serializeArray();

                _checkout.shipping.address = _.zipObject(_.map(formDataArray, 'name'), _.map(formDataArray, 'value'));

                if (form.find('.vh-js-make-default').is(':checked')) {
                    v.callApi('/address/add', {
                        data: _checkout.shipping.address,
                        success: function (response) {
                            if (response.success === true) {
                                console.log('address added to user addresses');
                                _checkout.shipping.address.address_id = response.data.id;
                            } else {
                                console.log('Could not save user shipping address!');
                            }
                        }
                    }).then(function () {
                        saveAddressToOrder().then(function () {
                            _checkout.shipping.valid = true;
                            initStep('shipping_details');
                        });
                    })
                }

                saveAddressToOrder().then(function () {
                    _checkout.shipping.valid = true;
                    initStep('shipping_details');
                });
            });
        }

        /**
         * Initialize Edit Shipping Validation
         */
        function initEditShippingValidation() {
            _formInstances.shippingModal.editAddress = initFormValidation('.vh-js-checkout-edit-shipping-address-form', {
                first_name: {
                    validators: {
                        notEmpty: {message: 'First name is required'}
                    }
                },
                last_name: {
                    validators: {
                        notEmpty: {message: 'Last name is required'}
                    }
                },
                address_1: {
                    validators: {
                        notEmpty: {message: 'Address line 1 is required'}
                    }
                },
                city: {
                    validators: {
                        notEmpty: {message: 'City is required'}
                    }
                },
                state: {
                    validators: {
                        notEmpty: {message: 'State is required'}
                    }
                },
                country: {
                    validators: {
                        notEmpty: {message: 'Country is required'}
                    }
                },
                zip: {
                    validators: {
                        notEmpty: {message: 'Zip code is required'},
                        regexp: {
                            regexp: /^[0-9]{3,5}$/,
                            message: 'Invalid zip code'
                        }
                    }
                }
            }, function () {

                var form = $('.vh-js-checkout-edit-shipping-address-form');
                var formDataArray = form.serializeArray();

                var address = _.zipObject(_.map(formDataArray, 'name'), _.map(formDataArray, 'value'));

                v.callApi('/address/update', {
                    data: address,
                    success: function (response) {
                        if (response.success === true) {
                            if (response.data.address.id.toString() === _checkout.shipping.address.address_id.toString()) {
                                _updateShippingAddressDisplay(response.data.address);
                            }
                            console.log('modal - address updated in user addresses');
                        } else {
                            console.log('modal - Could not update user shipping address!');
                        }
                    }
                }).then(function () {
                    _selectShipping();
                });
            });
        }

        /**
         * Initialize Edit Shipping Validation
         */
        function initAddShippingValidation() {
            _formInstances.shippingModal.addAddress = initFormValidation('.vh-js-checkout-add-shipping-address-form', {
                first_name: {
                    validators: {
                        notEmpty: {message: 'First name is required'}
                    }
                },
                last_name: {
                    validators: {
                        notEmpty: {message: 'Last name is required'}
                    }
                },
                address_1: {
                    validators: {
                        notEmpty: {message: 'Address line 1 is required'}
                    }
                },
                city: {
                    validators: {
                        notEmpty: {message: 'City is required'}
                    }
                },
                state: {
                    validators: {
                        notEmpty: {message: 'State is required'}
                    }
                },
                country: {
                    validators: {
                        notEmpty: {message: 'Country is required'}
                    }
                },
                zip: {
                    validators: {
                        notEmpty: {message: 'Zip code is required'},
                        regexp: {
                            regexp: /^[0-9]{3,5}$/,
                            message: 'Invalid zip code'
                        }
                    }
                }
            }, function () {

                var form = $('.vh-client-component-checkout .vh-js-checkout-add-shipping-address-form');
                var formDataArray = form.serializeArray();

                var address = _.zipObject(_.map(formDataArray, 'name'), _.map(formDataArray, 'value'));

                v.callApi('/address/add', {
                    data: address,
                    success: function (response) {
                        if (response.success === true) {
                            console.log('modal - address added to user addresses');
                            // Update the order address as the user might have intended
                            // to update it by adding a new one
                            _updateShippingAddressDisplay(response.data.address);
                        } else {
                            console.log('modal - Could not save user shipping address!');
                        }
                    }
                }).then(function () {
                    saveAddressToOrder().then(function () {
                        _selectShipping();
                    });
                });
            });
        }

        /**
         * Reset the add/edit shipping address form when the modal is closed
         *
         * @private
         */
        function _resetModalForms() {
            if (_formInstances.shippingModal.addAddress !== null) {
                _formInstances.shippingModal.addAddress.resetForm(true).destroy();
            }
            if (_formInstances.shippingModal.editAddress !== null) {
                _formInstances.shippingModal.editAddress.resetForm(true).destroy();
            }
        }

        /**
         * PayPal Buttons Integration
         *
         * @param response
         * @private
         */
        function _paypalModal(response) {
            $('.vh-js-payment-container').find('.vh-js-paypal-container').empty();

            v.loadScript('https://www.paypal.com/sdk/js?intent=capture&client-id='+response.data.client_id, function () {
                showLoading(false);
                paypal.Buttons({
                    style: {
                        layout: 'horizontal',
                        color: 'silver',
                        shape: 'rect',
                        label: 'pay'
                    },
                    createOrder: function(data, actions) {
                        return actions.order.create({
                            purchase_units: [{
                                amount: {
                                    value: response.data.order_total
                                }
                            }]
                        });
                    },
                    onApprove: function(data, actions) {
                        showLoading(false);

                        _checkout.payment.data = data;
                        _checkout.payment.valid = true;
                        _checkout.review.valid = true;

                        initStep('review');
                    },
                    onError: function (err) {
                        window.vahara.Alert.alert('Error!', err);
                    }
                }).render('.vh-js-paypal-container');

            }, true, true, true);
        }


        /**
         * Clover Modal
         * @private
         */
        function _cloverModal(response) {

            const style = document.createElement('style');
            style.textContent = `iframe { height: 100px !important; }`;
            document.head.appendChild(style);

            const cloverContainer = $('.vh-js-payment-container').find('.vh-js-clover-container');
            initCloverPaymentValidation(cloverContainer);
            var html = v.compileTemplate(_templates.cloverPaymentForm, []);
            cloverContainer.empty().append(html);
            const styles = {
                body: {
                    webkitFontSmoothing: 'antialiased',
                    mozOsxFontSmoothing: 'grayscale',
                },
		input: {
		    border: '1px solid #ccc;',
		    padding: '30px',
		    'font-size': '16px'
		}

            };
            v.loadScript('https://checkout.clover.com/sdk.js', function () {
                _checkout.clover.instance = new Clover(response.data.pub_token);
                _checkout.clover.elements = _checkout.clover.instance.elements();
                _checkout.clover.cardNumber = _checkout.clover.elements.create('CARD_NUMBER', styles);
                _checkout.clover.cardDate = _checkout.clover.elements.create('CARD_DATE', styles);
                _checkout.clover.cardCvv = _checkout.clover.elements.create('CARD_CVV', styles);
                _checkout.clover.cardPostCode = _checkout.clover.elements.create('CARD_POSTAL_CODE', styles);
                _checkout.clover.cardNumber.mount('#card-number');
                _checkout.clover.cardDate.mount('#card-date');
                _checkout.clover.cardCvv.mount('#card-cvv');
                _checkout.clover.cardPostCode.mount('#card-postal-code');
                _checkout.clover.fields = {
                    cardNumber: {
                        field: 'CARD_NUMBER',
                        error: '#card-number-errors',
                        valid: false
                    },
                    cardDate: {
                        field: 'CARD_DATE',
                        error: '#card-date-errors',
                        valid: false
                    },
                    cardCvv: {
                        field: 'CARD_CVV',
                        error: '#card-cvv-errors',
                        valid: false
                    },
                    cardPostCode: {
                        field: 'CARD_POSTAL_CODE',
                        error: '#card-postal-code-errors',
                        valid: false
                    }
                };
                _checkout.payment.valid = false;
                const _checkIsCardFormValid = function() {
                    return _checkout.clover.fields.cardNumber.valid &&
                        _checkout.clover.fields.cardDate.valid &&
                        _checkout.clover.fields.cardCvv.valid &&
                        _checkout.clover.fields.cardPostCode.valid;
                };
                for (const [fieldKey, fieldDetails] of Object.entries(_checkout.clover.fields)) {
                    // On Change
                    _checkout.clover[fieldKey].addEventListener('change', function(event) {
                        const errorFieldSelector = cloverContainer.find(fieldDetails['error']);
                        const fieldError = event[fieldDetails['field']].error;
                        if (typeof fieldError !== 'undefined') {
                            errorFieldSelector.html(fieldError);
                            _checkout.clover.fields[fieldKey].valid = false;
                        } else {
                            errorFieldSelector.html('');
                            _checkout.clover.fields[fieldKey].valid = true;
                        }
                        _checkout.payment.valid = _checkIsCardFormValid();
                    });
                    // On Blur
                    _checkout.clover[fieldKey].addEventListener('blur', function(event) {
                        const errorFieldSelector = cloverContainer.find(fieldDetails['error']);
                        const fieldError = event[fieldDetails['field']].error;
                        if (typeof fieldError !== 'undefined') {
                            errorFieldSelector.html(fieldError);
                            _checkout.clover.fields[fieldKey].valid = false;
                        } else {
                            errorFieldSelector.html('');
                            _checkout.clover.fields[fieldKey].valid = true;
                        }
                        _checkout.payment.valid = _checkIsCardFormValid();
                    });
                }
            }, true, true);
        }

        /**
         * Call Stripe payment modal (Updated to stripe elements)
         * @private
         */
        function _stripeModal(response) {

            initStripePaymentValidation();

            var html = v.compileTemplate(_templates.stripePaymentForm, []);
            $('.vh-js-payment-container').find('.vh-js-stripe-container').empty().append(html);

            v.loadScript('https://js.stripe.com/v3/', function () {
                _checkout.stripe.instance = Stripe(response.data.pub_token);
                _checkout.stripe.elements = _checkout.stripe.instance.elements();
                _checkout.stripe.card = _checkout.stripe.elements.create('card', {style: {
                    base: {
                        color: '#32325d',
                        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                        fontSmoothing: 'antialiased',
                        fontSize: '16px',
                        '::placeholder': {
                            color: '#aab7c4'
                        }
                    },
                    invalid: {
                        color: '#fa755a',
                        iconColor: '#fa755a'
                    }
                }});

                _checkout.stripe.card.mount('.vh-js-stripe-card-holder');

                _checkout.stripe.card.addEventListener('ready', function(event) {
                    showLoading(false);
                });

                // Handle real-time validation errors from the card Element.
                _checkout.stripe.card.addEventListener('change', function(event) {
                    _checkout.payment.valid = event.complete;
                    var errorsEl = $('.vh-js-stripe-container').find('.vh-js-stripe-card-errors');
                    if (event.error) {
                        errorsEl.html(event.error.message);
                    } else {
                        errorsEl.html('');
                    }
                });
            }, true, true);
        }

        /**
         * Call CMS payment modal
         *
         * @param response
         * @private
         */
        function _cmsModal(response) {
            var cmsIframe = document.createElement("iframe");
            var modal = $('.vh-client-component-checkout .vh-js-payment-modal');

            cmsIframe.scrolling = "no";
            cmsIframe.style.position = "absolute";
            cmsIframe.style.height = "100%";
            cmsIframe.seamless = "seamless";
            cmsIframe.src = response.iframeUrl;
            cmsIframe.onload = function () {
                window.setTimeout(function () {
                    showPaymentModal(true);
                }, 500);
            };

            modal.find('.vh-js-modal-body')
                .empty()
                .append(cmsIframe);

            window.addEventListener('message', handleSlyceResponse, false);

            function handleSlyceResponse(e) {
                if (e.origin === 'https://plugin.slycepay.com') {
                    showPaymentModal(false);

                    // Complete the order based on available payment data
                    // The completeOrder method will trigger SUCCESS or FAILED payment
                    // events
                    completeOrder(e.data);
                } else {
                    alert('Payment origin unknown!');
                }
            }
        }

        /**
         * Call CMS payment modal
         *
         * @param response
         * @private
         */
        function _nexioPayModal(response) {
            var nexioPayIframe = $('<iframe />');
            var targetContainer = $('.vh-client-component-checkout .vh-js-nexiopay-container');
            var targetForm = $('.vh-client-component-checkout .vh-js-nexiopay-form');

            nexioPayIframe.css({
                'display': 'none',
                'width': '100%',
                'height': '100%',
                'border': 'none',
                'minHeight': '1050px',
            });

            nexioPayIframe.attr({
                scrolling: 'no',
                seamless: 'seamless',
                src: response.data.iframeUrl
            });

            targetForm.hide();
            targetForm.off('submit');
            targetContainer.empty().append(nexioPayIframe);

            var iFrameUrl = response.data.iframeUrl;
            var iframeDomain = iFrameUrl.match(/^http(s?):\/\/.*?(?=\/)/)[0];
            window.addEventListener('message', function messageListener(event) {
                if (event.origin === iframeDomain) {
                    if (event.data.event === 'loaded') {
                        nexioPayIframe.show();
                        targetForm.show();

                        // On Iframe Loaded
                        showLoading(false);
                    }

                    if (event.data.event === 'error') {
                        // On Iframe Error
                        showLoading(false);

                        window.vahara.Alert.alert('Error', 'There was an error with the payment instrument. Please review and retry.');
                    }

                    if (event.data.event === 'cardSaved') {
                        // On Card Saved with event.data.data
                        showLoading(false);

                        _checkout.payment.valid = true;
                        _checkout.payment.data = event.data.data;
                        _checkout.review.valid = true;

                        initStep('review');
                    }

                    if (event.data.event === 'submit') {
                        // On Iframe Submit
                        showLoading(true);
                    }
                }
            });

            targetForm.on('submit', function processPayment(event) {
                event.preventDefault();

                if (!window.vahara.userId && !window.vahara.guestUser) {
                    v.jumpToAfterLogin = _settings.store_checkout_page;
                    window.vahara.triggerEvent('vh-login:show-guest-checkout-options');
                    return false;
                }

                nexioPayIframe[0].contentWindow.postMessage('posted', iFrameUrl);

                return false;
            });
        }

        /**
         * Save an address to order
         *
         * @returns {*|void}
         */
        function saveAddressToOrder() {
            return v.callApi('/orders/save-shipping-address', {
                data: {
                    visitor_id: v.getVisitorId(),
                    order_id: _orderId,
                    address: _checkout.shipping.address,
                    guest_checkout: parseInt(v.userId) > 0 ? 0 : 1
                },
                success: function (response) {
                    if (response.success === true) {
                        console.log('address saved to order');
                    } else {
                        console.log('Could not save shipping address to order!');
                    }
                }
            });
        }

        /**
         * Show the shipping modal with custom html
         *
         * @param html
         * @param callback
         * @private
         */
        function _showShippingModal(html, callback) {
            var modal = $('.vh-client-component-checkout .vh-js-shipping-mp-modal');
            modal.find('.vh-js-shipping-mp-wrapper').empty().append(html);
            setTimeout(function () {
                modal.modal('show');
                if (typeof callback === 'function') callback();
            }, 200);
        }

        /**
         * Hide the shipping modal
         *
         * @private
         */
        function _hideShippingModal() {
            var modal = $('.vh-client-component-checkout .vh-js-shipping-mp-modal');
            modal.modal('hide');
            modal.find('.vh-js-shipping-mp-wrapper').empty();
        }

        /**
         * Gets the selected shipping rate
         *
         * @returns {number}
         * @private
         */
        function _getSelectedShippingRate() {
            var shippingPrice = 0;
            var selectedShippingOption = $('.vh-client-component-checkout .vh-js-shipping-option-input:checked');
            if (selectedShippingOption.length > 0) {
                shippingPrice = selectedShippingOption.data('rate');
            }

            return shippingPrice;
        }

        /**
         * Open select shipping modal
         *
         * @private
         */
        function _selectShipping() {
            v.callApi('/address/get-addresses', {
                before: function () {
                    showLoading(true);
                },
                after: function () {
                    showLoading(false);
                },
                success: function (response) {
                    response.data.selected_shipping_id = _checkout.shipping.address.address_id;
                    var html = v.compileTemplate(_templates.shippingAddressList, response.data);
                    _showShippingModal(html);
                }
            });
        }

        /**
         * Open the add shipping modal
         *
         * @private
         */
        function _addShipping() {
            var html = v.compileTemplate(_templates.addShippingAddress);
            _showShippingModal(html, function () {
                initAddShippingValidation();
            });
        }

        /**
         * Open edit shipping modal
         *
         * @private
         */
        function _editShipping(address) {
            var html = v.compileTemplate(_templates.editShippingAddress, address);
            _showShippingModal(html, function () {
                initEditShippingValidation();
            });
        }

        /**
         * Update shipping address displayed on the payment step
         *
         * @param address
         * @private
         */
        function _updateShippingAddressDisplay(address) {
            _checkout.shipping.address = {
                address_id: address.id,
                first_name: address.first_name,
                last_name: address.last_name,
                address1: address.address_1,
                address2: address.address_2,
                address_1: address.address_1,
                address_2: address.address_2,
                city: address.city,
                country: address.country,
                state: address.state,
                zip: address.zip,
                phone: address.phone,
            };

            var html = v.compileTemplate(_templates.partials.address, _checkout.shipping.address);

            $('.vh-client-component-checkout')
                .find('.vh-js-shipto-address')
                .html(html);
        }

        /**
         * Update item quantity based on `+` and `-` on side cart
         *
         * @param itemId
         * @param action
         * @returns {boolean}
         */
        function updateItemQuantity(itemId, action) {
            if (typeof action === 'undefined') action = 'I';
            if (typeof itemId === 'undefined' || itemId === null) return false;

            var product = _.find(_addedProducts, function (o) {
                return o.id.toString() === itemId.toString();
            });

            if (typeof product !== undefined) {
                var newQty = action === 'I' ? product.quantity + 1 : product.quantity - 1;
                if (newQty < 0) newQty = 0;

                // Just in case where we may have not changes the qty in any way
                if (product.quantity === newQty) return false;

                if (newQty > 0) {
                    addOrUpdateCartItem(itemId, newQty);
                } else {
                    removeCartItem(itemId);
                }
            }
        }

        /**
         * Update cart item
         *
         * @param itemId
         * @param quantity
         * @returns {boolean}
         */
        function addOrUpdateCartItem(itemId, quantity) {
            if (typeof quantity === 'undefined' || quantity === null || quantity <= 0) return false;
            if (typeof itemId === 'undefined' || itemId === null || itemId <= 0) return false;

            v.callApi('/cart/add-to-cart', {
                data: {
                    visitor_id: v.getVisitorId(),
                    item_id: itemId,
                    quantity: quantity
                },
                before: function () {
                    showLoading(true);
                },
                after: function () {
                    showLoading(false);
                },
                success: function (response) {
                    if (response.success === true) {
                        if (response.message === 'added') {

                            // Find the index of the added element in _addedProducts
                            var addedProductIndex = _.findIndex(_addedProducts, function (o) {
                                return o.id.toString() === response.data.orderItem.item_id.toString();
                            });

                            // Make the _item definition standard
                            var _item = {
                                id: response.data.orderItem.item_id,
                                product_sku: response.data.orderItem.details.sku,
                                product_title: response.data.orderItem.name,
                                product_cart_label: response.data.orderItem.details.product_cart_label,
                                product_image: response.data.orderItem.details.style_image,
                                product_price: response.data.orderItem.price,
                                quantity: response.data.orderItem.quantity,
                                discount: response.data.orderItem.discount,
                                volume: response.data.orderItem.volume
                            };

                            // Update the item object
                            _addedProducts.splice(addedProductIndex, 1, _item);

                            // Trigger the item-added or item-updated event
                            v.triggerEvent('vh-checkout:item-updated', {item: _item});
                        }
                    }
                }
            });
        }

        /**
         * Remove a cart item
         *
         * @param itemId
         */
        function removeCartItem(itemId) {
            if (itemId > 0 && itemId !== 'undefined' && itemId !== null) {
                v.callApi('/cart/remove-from-cart', {
                    data: {
                        visitor_id: v.getVisitorId(),
                        item_id: itemId
                    },
                    before: function () {
                        showLoading(true);
                    },
                    after: function () {
                        showLoading(false);
                    },
                    success: function (response) {
                        if (response.success === true) {
                            // Trigger the item-removed event
                            v.triggerEvent('vh-checkout:item-removed', {item: response.data.orderItem});
                        }
                    }
                });
            } else {
                console.error('Invalid product!');
            }
        }

        /**
         * Complete the order whether failed/succeeded
         *
         * @param paymentData
         */
        function completeOrder(paymentData) {
            return v.callApi('/orders/complete', {
                data: {
                    visitor_id: v.getVisitorId(),
                    order_id: _orderId,
                    payment_info: paymentData
                },
                success: function (response) {
                    if (response.success === true) {
                        // Trigger payment approved event
                        v.triggerEvent('vh-checkout:payment-success', {
                            data: response
                        });

                        // Run interactions
                        if(response.data.interaction_results) {
                            runInteractions(response.data.interaction_results);
                        }

                        if(window.vahara.guestUser) {
                            v.jQuery.ajax('/logout_callback');
                            v.triggerEvent('vh-login:status-updated');
                        }
                    } else {
                        // Trigger payment failed event
                        v.triggerEvent('vh-checkout:payment-failed', {
                            data: paymentData
                        });
                    }
                },
                after: function () {
                    showLoading(false);
                }
            });
        }

        /**
         * Fetch user cart items
         *
         * @param callback
         */
        function getCartItems(callback) {
            v.callApi('/cart/get-cart', {
                data: {
                    visitor_id: v.getVisitorId(),
                    guest_checkout: parseInt(v.userId) > 0 ? 0 : 1
                },
                success: function (response) {
                    if (response.success === true) {
                        _orderId = response.data.order_id;
                        _addedProducts = response.data.items;
                        if (typeof callback === 'function') callback(response.data);
                    } else {
                        console.log('No items added to cart!');
                    }
                }
            });
        }

        /**
         * Render the side cart display
         */
        function renderSideBox(shippingAmount) {
            getCartItems(function (cart) {
                if (cart.items.length > 0) {
                    var _sidebarData = {
                        cart_data: cart,
                        total_items: 0,
                        shipping_amount: 0,
                        subtotal: cart.totals.product_total,
                        order_total: 0,
                        total_discount: 0,
                        should_ship: !_checkout.order.non_shipping
                    };

                    _.forEach(cart.items, function (o) {
                        _sidebarData.total_items += parseInt(o.quantity);
                        _sidebarData.total_discount += parseInt(o.quantity) * parseInt(o.discount);
                    });

                    $('.vh-js-cart-qty').text(_sidebarData.total_items);

                    // If the shipping amount that we got for the order is 0, that means the shipping has not been
                    // saved to the order yet
                    shippingAmount = _checkout.shipping.amount;

                    if (typeof shippingAmount !== undefined && shippingAmount !== null && !isNaN(shippingAmount)) {
                        // Override order total and shipping amount
                        _sidebarData.shipping_amount = shippingAmount;
                        _sidebarData.order_total = cart.totals.product_total + shippingAmount;
                    }

                    //Minus the discount amount from total.
                    if (_sidebarData.total_discount) {
                        _sidebarData.order_total = _sidebarData.order_total - _sidebarData.total_discount;
                    }

                    var sideBox = $('.vh-client-component-checkout .vh-js-sidebox');
                    var scrollTop = sideBox.find('.vh-product-scroll-box').scrollTop();

                    console.log('sidebox');
                    console.log(_sidebarData);
                    var html = v.compileTemplate(_templates.sideBox, _sidebarData);
                    sideBox
                        .empty()
                        .append(html)
                        .find('.vh-product-scroll-box')
                        .scrollTop(scrollTop);
                } else {
                    v.triggerEvent('vh-checkout:cart-empty');
                }
            });
        }

        /**
         * Switches to a specific step
         *
         * @param toStep
         */
        function switchStep(toStep) {
            if(steps.indexOf(toStep) !== -1) {
                $('.vh-client-component-checkout .vh-js-step-switch[data-step]')
                    .attr('disabled', 'disabled')
                    .removeClass('vh-step-active');
                $('.vh-client-component-checkout .vh-js-step-switch[data-step*="' + toStep + '"]')
                    .addClass('vh-step-active');
                $('.vh-client-component-checkout .vh-js-step[data-step]')
                    .removeClass('vh-step-content-active');
                $('.vh-client-component-checkout .vh-js-step[data-step*="' + toStep + '"]')
                    .addClass('vh-step-content-active');

                _checkout.currentStep = toStep;

                setTimeout(function () {
                    $('html, body').animate({scrollTop: 0}, 'slow');
                }, 800);
            }
        }

        /**
         * Show/Hide loading overlay
         *
         * @param show
         */
        function showLoading(show) {
            if (typeof show === 'undefined') show = true;
            if (show) {
                $('.vh-client-component-checkout .vh-submit-form').attr('disabled', 'disabled');
                $('.vh-client-component-checkout').addClass('loading');
            } else {
                setTimeout(function () {
                    $('.vh-client-component-checkout .vh-submit-form').removeAttr('disabled');
                    $('.vh-client-component-checkout').removeClass('loading');
                }, 300);
            }
        }

        /**
         * Show/Hide payment modal
         *
         * @param show
         */
        function showPaymentModal(show) {
            if (typeof show === 'undefined') show = true;
            var modal = $('.vh-client-component-checkout .vh-js-payment-modal');
            if (show) {
                modal.addClass('visible');
            } else {
                modal.removeClass('visible');
            }
        }

        /**
         * Tracks which steps have been completed and updates the progress
         */
        function updateStepProgressUI() {
            for(var i in steps) {
                var oStep = steps[i];
                var step = steps[i];
                if(step === _checkout.currentStep) break;
                if(step === 'shipping_address' || step === 'shipping_details') step = 'shipping';
                if(step === 'select_card') step = 'payment';

                var stepHasCompleted = false;
                if(step === 'shipping') {
                    stepHasCompleted = _checkout[step].valid && _checkout[step].method !== null;
                } else {
                    stepHasCompleted = _checkout[step].valid;
                }

                if(stepHasCompleted || _checkout.currentStep == 'order_complete') {
                    $('.vh-client-component-checkout .vh-js-step-switch[data-step*="' + oStep + '"]').addClass('vh-step-done');
                }
            }
        }

        /**
         * Removes the shipping step when required
         */
        function removeShippingStep() {
            var step = 1;
            $('.vh-client-component-checkout li[data-step="shipping"]').remove();
            $('.vh-client-component-checkout .bootstrapWizard li').css('width', '33%');
            $('.vh-client-component-checkout li[data-step]').each(function() {
                $(this).find('span.step').text(step);
                step++;
            })
        }

        /**
         * Used when we're dealing with non shipping orders
         * @returns {*|jQuery}
         */
        function updateShippingMethodAndLoadToken()
        {
            var deferred = $.Deferred();

            var request = v.callApi('/orders/update-shipping', {
                data: {
                    visitor_id: v.getVisitorId(),
                    order_id: _orderId,
                    shipping_method: _checkout.shipping.method,
                    shipping_amount: _checkout.shipping.amount,
                    guest_checkout: parseInt(v.userId) > 0 ? 0 : 1
                }
            });

            request.then(function () {
                v.callApi('/payment/user-payments', {
                    before: function() { showLoading(true); },
                    success: function (response) {
                        showLoading(false);
                        if(response.success === true && response.data.length > 0) {
                            _checkout.payment.savedCards = response.data;
                            _checkout.payment.processor = _settings.payments_platform;
                        }
                    }
                }).then(function () {
                    showLoading(true);
                    if (_checkout.payment.savedCards.length > 0) {
                        initStep('select_card');
                    } else {
                        getTokenForNewPayment();
                    }
                })
            });

            return deferred.promise();
        }

        function runInteractions(interactions) {
            var interaction = null;
            for (var i in interactions) {
                interaction = interactions[i];
                if (interaction.message) {
                    window.vahara.Alert.alert('Message', interaction.message);
                }
                if (interaction.javascript) {
                    $.globalEval(interaction.javascript);
                }
                if (interaction.link) {
                    window.location = interaction.link;
                }
            }
        }

        var _switchPaymentPlatformSection = function(platform) {
            $('.vh-js-payment-container').hide();
            $('.vh-js-payment-container[data-platform="'+platform+'"]').show();
        };

        // Get cart items when loaded
        getCartItems(function (cart) {
            _settings = v.settings;
            if (cart.items.length > 0) {
                if(typeof cart.non_shipping !== 'undefined' && cart.non_shipping === true) {
                    _checkout.order.non_shipping = true;
                    _checkout.shipping.method = 'not_applicable';
                    _checkout.shipping.amount = 0;
                    removeShippingStep();
                    updateShippingMethodAndLoadToken();
                } else {
                    initStep('shipping_address');
                }
            } else {
                v.triggerEvent('vh-checkout:cart-empty');
            }
        });
    };
}

function initVaharaCheckout() {
    $$ = window.jQuery;

    var root = $$('.vh-element-checkout');
    var path = window.vahara.clientJqueryBootstrap4Path;

    if (window.vaharaLibsResourcePath) {
        path = window.vaharaLibsResourcePath;
    }

    if (window.vaharaCheckoutResourcePath) {
        path = window.vaharaCheckoutResourcePath;
    } else {
        path += '/checkout';
    }
    var template = path + '/checkout.tpl?r='+Math.random();
    if (window.vaharaCheckoutHtmlLocation) {
        template = window.vaharaCheckoutHtmlLocation;
    }
    var css = path + '/checkout.css';

    if (root.length > 0) {
        if (!window.vaharaLibsNoAutoLoadCss && !window.vaharaCheckoutLibsNoAutoLoadCss) {
            $$('head').append('<link id="client_checkout_css" rel="stylesheet" href="' + css + '" type="text/css">');
        }

        if (!window.vaharaLibsNoAutoRun && !window.vaharaCheckoutNoAutoRun) {
            if (!window.vaharaLibsNoAutoLoadHtml && !window.vaharaCheckoutLibsNoAutoLoadHtml) {
                $$.get(template, function (data) {
                    root.empty().append(data);

                    (new VaharaCheckout($$)).init();
                });
            } else {
                (new VaharaCheckout($$)).init();
            }
        }
    }

    window.removeEventListener('load', initVaharaCheckout, false);
}

if (!window.vahara.clientJqueryBootstrap4Path) {
    window.vahara.clientJqueryBootstrap4Path = window.vahara.getCurrentScriptPath(-2);
}

window.addEventListener('load', initVaharaCheckout, false);
