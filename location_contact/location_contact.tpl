<div class="vh-client-component-location_contact">

    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-2">
                <form class="vh-location-contact-search-form">
                    <input type="text" class="vh-element-location-contact-search-terms mr-2">
                    <button type="submit"
                            class="vh-element-location-contact-search-submit btn btn-sm btn-outline-secondary">SEARCH
                    </button>
                </form>
            </div>
            <div class="col-md-12">
                <div class="vh-location-contact-section vh-js-location-contacts-section"></div>
            </div>
        </div>
    </div>

    <!-- Location contact user modal -->
    <div class="modal fade vh-location-contact-modal vh-js-location-contact-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center">Contact</h4>
                    <button type="button" class="close vh-js-btn-close-location-contact-modal" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form method="post" class="form-horizontal vh-js-location-contact-modal-form">
                        <div class="vh-js-contact-info mb-3 text-center"></div>
                        <h5 class="mb-2">Get in touch:</h5>

                        <div class="form-group">
                            <label class="form-control-label">Name</label>
                            <div>
                                <input placeholder="Name" type="text" class="form-control vh-js-name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <div>
                                <input type="email" class="form-control vh-js-email" placeholder="Email Address">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Phone Number</label>
                            <div>
                                <input type="tel" class="form-control vh-js-phone" placeholder="Phone Number">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Best Time to Call</label>
                            <div>
                                <input placeholder="best time to call" type="text" class="form-control vh-js-best-time-to-call">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Message</label>
                            <div>
                                <textarea class="form-control vh-js-message"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class=" col-md-offset-4">
                                <button type="button" class="btn btn-outline-dark pull-right vh-js-btn-close-location-modal" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-outline-secondary pull-right mr-2">Send</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--end Location contact user modal-->

    <!-- TEMPLATES -->

    <!---------------------------
        Contact Modal Snippet
    -----------------------------
        id
        name
        email
        user_id
        project_id
        address_1
        address_2
        phone
        state
        street
        latitude
        longitude
        city
        zip
        about
        link
    -->
    <script class="vh-js-template-contact-details" data-priority="100" type="text/x-handlebars-template">
        <input type="hidden" class="vh-js-contact-person-id" value="{{id}}">
        <h5 class="vh-contact-person-name mb-3 text-center">{{name}}</h5>
        <h5 class="vh-contact-person-phone mb-3 text-center">{{phone}}</h5>
        <h6 class="vh-contact-person-about mb-4 text-center">{{about}}</h6>
        {{#if link}}
            <h6 class="mb-4 text-center">
                {{#if link_text}}
                    <a href="{{link}}" class="vh-contact-person-website-link" target="_blank">{{link_text}}</a>
                {{else}}
                    <a href="{{link}}" class="vh-contact-person-website-link" target="_blank">{{link}}</a>
                {{/if}}
            </h6>
        {{/if}}
    </script>

</div>


