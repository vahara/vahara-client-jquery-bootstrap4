function VaharaLocationContact($) {

    this.init = function (rootElement) {

        // Don't initialize in edit mode
        if (window.vahara.editMode) { return; }

        console.log("VaharaLocationContact Initialized!");

        var defaultZoom = 1.005;
        var defaultSearchZoom = null;
        var locationContactMap = null;
        var locationContactMapBounds = null;
        var geoCoder = null;
        var contactForm = null;

        defaultZoom = rootElement.data('default-zoom') ? rootElement.data('default-zoom') : 3;
        defaultSearchZoom = rootElement.data('default-search-zoom') ? rootElement.data('default-search-zoom') : null;

        v.loadGoogleMaps(initializeContactMap);

        initLocationContactFormValidation();

        $(document).on('click', '.vh-js-btn-close-location-contact-modal, .close', function () {
            resetLocationContactForm();
        });

        var $submitElement = $(".vh-element-location-contact-search-submit");

        if ($submitElement.length) {
            $(".vh-location-contact-search-form").submit(function (event) {
                event.preventDefault();
                let searchTerm = $(".vh-element-location-contact-search-terms").val();
                v.triggerEvent("vh-location:find", {
                    detail: {
                        locations: searchTerm
                    }
                });
            });

            /**
             * Handle the location lisiting in map
             */
            v.onEvent("vh-location:find", function (data) {
                findAddress(data.detail.locations);
            });
        }

        function initializeContactMap() {
            locationContactMap = new GMaps({
                div: ".vh-js-location-contacts-section",
                lat: 36,
                lng: -96,
                zoom: defaultZoom,
                disableDefaultUI: true,
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE,
                    position: google.maps.ControlPosition.RIGHT_CENTER
                },
                fullscreenControl: true
            });

            locationContactMapBounds = new google.maps.LatLngBounds();

            geoCoder = new google.maps.Geocoder();

            fetchUsersLocation();
        }

        function initLocationContactFormValidation() {
            var validationElement = $(".vh-js-location-contact-modal-form")[0];

            contactForm = FormValidation.formValidation(validationElement, {
                fields: {
                    "vh-js-name": {
                        selector: ".vh-js-name",
                        validators: {
                            notEmpty: {
                                message: "Please enter your name"
                            }
                        }
                    },
                    "vh-js-email": {
                        selector: ".vh-js-email",
                        validators: {
                            notEmpty: {
                                message: "Please enter an email address"
                            },
                            emailAddress: {
                                message: "Please enter an email address"
                            }
                        }
                    },
                    "vh-js-phone": {
                        selector: ".vh-js-phone",
                        validators: {
                            notEmpty: {
                                message: "Please enter your phone number"
                            }
                        }
                    },
                    "vh-js-best-time-to-call": {
                        selector: ".vh-js-best-time-to-call",
                        validators: {
                            notEmpty: {
                                message: "Please enter a best time to call"
                            }
                        }
                    },
                    "vh-js-message": {
                        selector: ".vh-js-message",
                        validators: {
                            notEmpty: {
                                message: "Please enter a mesage"
                            }
                        }
                    }
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    submitButton: new FormValidation.plugins.SubmitButton()
                }
            }).on("core.form.valid", function () {
                v.callApi("/location/send-mail", {
                    data: {
                        location_id: $(".vh-js-contact-person-id", ".vh-js-location-contact-modal-form").val(),
                        name: $(".vh-js-name", ".vh-js-location-contact-modal-form").val(),
                        email: $(".vh-js-email", ".vh-js-location-contact-modal-form").val(),
                        phone: $(".vh-js-phone", ".vh-js-location-contact-modal-form").val(),
                        bestTimeToCall: $(".vh-js-best-time-to-call", ".vh-js-location-contact-modal-form").val(),
                        message: $(".vh-js-message", ".vh-js-location-contact-modal-form").val()
                    },
                    success: function (response) {
                        if (response.success) {
                            $(".vh-js-location-contact-modal").modal("toggle");
                            $(".vh-js-btn-close-location-contact-modal").click();
                        } else if (response.message && response.message.length > 0) {
                            window.vahara.Alert.alert("Error", response.message);
                        }
                    }
                });
            });
        }

        function resetLocationContactForm() {
            if (contactForm) {
                contactForm.resetForm();
            }

            $(".vh-js-location-contact-modal-form")
                .find("input,textarea,select")
                .val("")
                .end()
                .find("input[type=checkbox], input[type=radio]")
                .prop("checked", "")
                .end();
        }

        function fetchUsersLocation() {
            v.callApi("/location/get-all", {
                success: function (response) {
                    if (response.success) {
                        if (response.data && response.data.locations && response.data.locations.length > 0) {
                            renderUsersLocations(response.data.locations);
                        }
                    } else if (response.message && response.message.length > 0) {
                        window.vahara.Alert.alert("Error", response.message);
                    }

                    getCurrentLatLong();
                }
            });
        }

        function renderUsersLocations(locations) {
            if (typeof locationContactMap !== "undefined") {
                locationContactMap.removeMarkers();
            }

            locationContactMapBounds = new google.maps.LatLngBounds();

            $.each(locations, function (key, location) {
                showContactLocationMarker(location);
            });
        }

        function showContactLocationMarker(location) {
            if (location.latitude && location.longitude) {
                let marker = locationContactMap.addMarker({
                    lat: location.latitude,
                    lng: location.longitude,
                    position: new google.maps.LatLng(
                        location.latitude,
                        location.longitude
                    )
                });

                google.maps.event.addListener(marker, "click", function () {
                    showContactForm(location.id);
                });

                locationContactMapBounds.extend(marker.position);
                locationContactMap.setCenter(
                    location.latitude,
                    location.longitude
                );
            }
        }

        function showContactForm(locationId) {
            v.callApi("/location/contact-details", {
                data: {id: locationId},
                success: function (response) {
                    if (response.success) {
                        if (response.data && response.data.location != null) {
                            var locationObject = response.data.location;
                            if (locationObject.link != null && locationObject.link !== "") {
                                var link = locationObject.link;
                                locationObject.link_text = link.replace(/(^\w+:|^)\/\//, "");
                            }
                            var contactInfoHtml = v.compileTemplate('.vh-js-template-contact-details', locationObject);
                            var modal = $(".vh-js-location-contact-modal");

                            modal.find('.vh-js-contact-info').empty().append(contactInfoHtml);
                            modal.modal("show");
                        }
                    } else if (response.message && response.message.length > 0) {
                        window.vahara.Alert.alert("Error", response.message);
                    }
                }
            });
        }

        /**
         * Gets current geolocation
         * to set users location center if no location find
         */
        function getCurrentLatLong() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    locationContactMap.setCenter({
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    });
                    locationContactMap.zoomIn(5);
                }, function () {
                    locationContactMapBounds = new google.maps.LatLngBounds();
                });
            } else {
                // IF NOT FIND GEOLOCATION
                locationContactMapBounds = new google.maps.LatLngBounds();
            }
        }

        /* redirect the location to users location*/
        function redirectLocation(position) {
            var pos = {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude
            };

            locationContactMap.setCenter(pos.latitude, pos.longitude);
            locationContactMap.zoomIn(defaultZoom);
        }

        function errorHandler(positionError) {
            if (window.console) {
                console.log(positionError);
            }
        }

        //find the location as per entered address
        function findAddress(address) {
            geoCoder.geocode({address: address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    //Got result, center the map and put it out there
                    var pos = {
                        latitude: results[0].geometry.location.lat(),
                        longitude: results[0].geometry.location.lng()
                    };

                    locationContactMap.setCenter(pos.latitude, pos.longitude);
                    locationContactMap.fitBounds(results[0].geometry.viewport);

                    /* IF USER SET DEFAULT SEARCH ZOOM LEVEL*/
                    if (defaultSearchZoom) {
                        locationContactMap.zoomIn(defaultSearchZoom);
                    }
                } else {
                    window.vahara.Alert.alert("Error", "No location found.");
                }
            });
        }
    };
}

function initVaharaLocationContact() {
    $$ = window.jQuery;

    if (!window.vahara.clientJqueryBootstrap4Path) {
        var scriptParts = window.vhLocationContactCurrentScript.split('/');
        window.vahara.clientJqueryBootstrap4Path = scriptParts.slice(0, scriptParts.length - 2).join('/');
    }

    // Don't initialize in edit mode
    if (window.vahara.editMode) { return; }

    var root = $$(".vh-element-location_contact");
    var path = window.vahara.clientJqueryBootstrap4Path;

    if (window.vaharaLibsResourcePath) {
        path = window.vaharaLibsResourcePath;
    }

    if (window.vaharaLocationContactResourcePath) {
        path = window.vaharaLocationContactResourcePath;
    } else {
        path += "/location_contact";
    }

    var template = path + "/location_contact.tpl";
    if (window.vaharaLocationContactHtmlLocation) {
        template = window.vaharaLocationContactHtmlLocation;
    }

    var css = path + "/location_contact.css";

    if (root.length > 0) {
        if (!window.vaharaLibsNoAutoLoadCss && !window.vaharaLocationContactNoAutoLoadCss) {
            $$("head").append('<link id="client_location_contact_css" rel="stylesheet" href="' + css + '" type="text/css">');
        }

        if (!window.vaharaLibsNoAutoRun && !window.vaharaLocationContactNoAutoRun) {
            if (!window.vaharaLibsNoAutoLoadHtml && !window.vaharaLocationContactNoAutoLoadHtml) {
                $$.get(template, function (data) {
                    root.empty().append(data);

                    new VaharaLocationContact($$).init(root);
                });
            } else {
                new VaharaLocationContact($$).init(root);
            }
        }
    }

    window.removeEventListener("load", initVaharaLocationContact, false);
}

window.vhLocationContactCurrentScript = document.currentScript.src;
window.addEventListener("load", initVaharaLocationContact, false);
